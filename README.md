# RPG Character Optimizaer

## Compilation

Run the following command:
```bash
mvn compile
```

## Running

Run the following command:
```bash
mvn exec:java -Dexec.mainClass=Main
```

## Configuration

Change the config.json file in the project folder

## Data

Load your data into a testdata folder
```bash
mkdir testdata
cd testdata
```
And put your files with the names inside the config.json