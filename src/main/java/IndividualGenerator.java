import ConcreteGenes.EquipmentGene;
import ConcreteGenes.HeightGene;
import Genes.Gene;
import Individuals.Individual;
import Operators.CrossOperator;
import Operators.FitnessCalculator;
import Operators.MutationOperator;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.floor;
import static java.lang.Math.random;

public class IndividualGenerator {

    private FitnessCalculator fc;
    private  MutationOperator mo;
    private     CrossOperator co;
    private CharacterConfig cc;

    public IndividualGenerator(FitnessCalculator fc, MutationOperator mo, CrossOperator co, CharacterConfig cc) {
        this.fc = fc;
        this.mo = mo;
        this.co = co;
        this.cc = cc;
    }

    public List<Individual> generatePopulation(int n) {
        List<Individual> ret = new ArrayList<>(n);

        for(int i = 0; i < n; i++) {
                Gene[] chromosome = generateChromosome();
            Individual       next = new Individual(chromosome, fc, mo, co);

            ret.add(next);
        }
        return ret;
    }

    private Gene[] generateChromosome() {
        //Get random height
            double height = random()*(2.0 - 1.3) + 1.3;
        HeightGene     hg = new HeightGene(height);

        //Get random weapon
                  int  index = (int)floor(random()*(cc.getWeapons().length));
        EquipmentGene weapon = cc.getWeapons()[index];
        //Get random boots
                      index = (int)floor(random()*(cc.getBoots().length));
        EquipmentGene boots = cc.getBoots()[index];
        //Get random helmet
                       index = (int)floor(random()*(cc.getHelmets().length));
        EquipmentGene helmet = cc.getHelmets()[index];
        //Get random gauntlets
                          index = (int)floor(random()*(cc.getGauntlets().length));
        EquipmentGene gauntlets = cc.getGauntlets()[index];
        //Get random chestPiece
                           index = (int)floor(random()*(cc.getChestPieces().length));
        EquipmentGene chestPiece = cc.getChestPieces()[index];

        Gene[] ret = {hg, weapon, boots, helmet, gauntlets, chestPiece};

        return ret;
    }

}
