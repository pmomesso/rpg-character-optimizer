package Genes;

public abstract class Gene {

    public abstract Gene mutate();
    public abstract Gene copy();

}
