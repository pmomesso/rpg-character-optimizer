package ConcreteCrossOperators;

import Genes.Gene;

public class CrossOperatorsUtils {

    public static Gene[] copyChromosome(Gene[] chromosome) {
        Gene[] ret = new Gene[chromosome.length];
        for(int i = 0; i < chromosome.length; i++) {
            ret[i] = chromosome[i].copy();
        }
        return ret;
    }

}
