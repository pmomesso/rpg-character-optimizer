package ConcreteCrossOperators;

import Genes.Gene;
import Individuals.Individual;
import Operators.CrossOperator;
import Operators.FitnessCalculator;
import Operators.MutationOperator;

import static ConcreteCrossOperators.CrossOperatorsUtils.copyChromosome;
import static java.lang.Math.floor;

public class DoublePointCrossOperator implements CrossOperator {
    @Override
    public Individual[] crossIndividuals(Gene[] chromosomes1, Gene[] chromosomes2, FitnessCalculator fc, MutationOperator mo, CrossOperator co) {
        int locus1 = (int)floor(Math.random()*(chromosomes1.length));
        int locus2 = (int)floor(Math.random()*(chromosomes1.length));
        if(locus1 > locus2) {
            int aux = locus1;
            locus1 = locus2;
            locus2 = aux;
        }
        Gene[] newChromosome1 = copyChromosome(chromosomes1);
        Gene[] newChromosome2 = copyChromosome(chromosomes2);
        for(int i = locus1; i < locus2; i++) {
                 Gene auxGene = newChromosome1[i];
            newChromosome1[i] = newChromosome2[i];
            newChromosome2[i] = auxGene;
        }
        Individual ret1 = new Individual(newChromosome1, fc, mo, co);
        Individual ret2 = new Individual(newChromosome2, fc, mo, co);
        Individual[] ret = {ret1, ret2};
        return ret;
    }
}
