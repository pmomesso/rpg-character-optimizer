package ConcreteCrossOperators;

import Genes.Gene;
import Individuals.Individual;
import Operators.CrossOperator;
import Operators.FitnessCalculator;
import Operators.MutationOperator;

import static ConcreteCrossOperators.CrossOperatorsUtils.copyChromosome;
import static java.lang.Math.random;

public class UniformCrossOperator implements CrossOperator {

    private double crossProbability;

    public UniformCrossOperator(double crossProbability) {
        this.crossProbability = crossProbability;
    }

    @Override
    public Individual[] crossIndividuals(Gene[] chromosomes1, Gene[] chromosomes2, FitnessCalculator fc, MutationOperator mo, CrossOperator co) {
        Gene[] newChromosome1 = copyChromosome(chromosomes1);
        Gene[] newChromosome2 = copyChromosome(chromosomes2);

        for(int i = 0; i < newChromosome1.length; i++) {
            if(Double.compare(random(), crossProbability) <= 0) {
                      Gene auxGene = newChromosome1[i];
                 newChromosome1[i] = newChromosome2[i];
                 newChromosome2[i] = auxGene;
            }
        }

        Individual  ret1 = new Individual(newChromosome1, fc, mo, co);
        Individual  ret2 = new Individual(newChromosome2, fc, mo, co);
        Individual[] ret = {ret1, ret2};
        return ret;
    }
}
