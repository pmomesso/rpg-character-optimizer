package ConcreteCrossOperators;

import Genes.Gene;
import Individuals.Individual;
import Operators.CrossOperator;
import Operators.FitnessCalculator;
import Operators.MutationOperator;

import static ConcreteCrossOperators.CrossOperatorsUtils.*;
import static java.lang.Math.floor;
import static java.lang.Math.random;

public class RingCrossOperator implements CrossOperator {

    private int length;

    public RingCrossOperator(int length) {
        this.length = length;
    }

    @Override
    public Individual[] crossIndividuals(Gene[] chromosomes1, Gene[] chromosomes2, FitnessCalculator fc, MutationOperator mo, CrossOperator co) {
        Gene[] newChromosome1 = copyChromosome(chromosomes1);
        Gene[] newChromosome2 = copyChromosome(chromosomes2);

        int     locus = (int) floor(random() * (chromosomes1.length));
        int exchanged = 0;
        while(exchanged < length) {
            Gene auxGene = newChromosome1[locus];
            newChromosome1[locus] = newChromosome2[locus];
            newChromosome2[locus] = auxGene;
                           locus = (locus + 1)%(newChromosome1.length);
            exchanged++;
        }

        Individual ret1 = new Individual(newChromosome1, fc, mo, co);
        Individual ret2 = new Individual(newChromosome2, fc, mo, co);
        Individual[] ret = {ret1, ret2};
        return ret;
    }

}