package StopCriteria;

public enum StopCriteriaType {
    TIME("time"),
    GENERATIONS("generations"),
    ACCEPTABLE("acceptable"),
    STRUCTURE("structure"),
    CONTENT("content");

    final private String reference;

    StopCriteriaType(String reference){
        this.reference = reference;
    }

    public String getReference() {
        return reference;
    }
}
