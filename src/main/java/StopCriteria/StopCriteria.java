package StopCriteria;

import Individuals.Individual;

import java.util.List;

public abstract class StopCriteria {
    final private StopCriteriaType stopCriteriaType;

    public StopCriteria(StopCriteriaType stopCriteriaType){
        this.stopCriteriaType = stopCriteriaType;
    }

    public StopCriteriaType getStopCriteriaType() {
        return stopCriteriaType;
    }

    public abstract boolean checkIfCriteriaIsMet(List<Individual> population, Individual ret, int currGeneration);
}
