package StopCriteria;

import Genes.Gene;
import Individuals.Individual;

import java.util.*;

import static java.lang.Math.*;

public class StructureStopCriteria extends StopCriteria {

    final private double     EPSILON_MEAN = 0.05;
    final private double EPSILON_VARIANCE = 0.05;
    final private Set<GeneType> importantGenes;
    final private int repeatedIndividuals;
    final private int maxGenerationsWithRepeats;

    private Map<Gene, Integer> amounts = new HashMap<>();
    private Set<Gene> shared = new HashSet<>();
    private double meanFitness = 0.0;
    private double    variance = 0.0;
    private    int generations = 0;
    private    int     minimum = 4;

    public StructureStopCriteria(Collection<GeneType> importantGenes, int repeatedIndividuals, int maxGenerationsWithRepeats){
        super(StopCriteriaType.STRUCTURE);
        this.importantGenes            = new HashSet<>(importantGenes);
        this.repeatedIndividuals       = repeatedIndividuals;
        this.maxGenerationsWithRepeats = maxGenerationsWithRepeats;
    }

    public Set<GeneType> getImportantGenes() {
        return importantGenes;
    }

    public int getRepeatedIndividuals() {
        return repeatedIndividuals;
    }

    public int getMaxGenerationsWithRepeats() {
        return maxGenerationsWithRepeats;
    }

    @Override
    public boolean checkIfCriteriaIsMet(List<Individual> population, Individual ret, int currGeneration) {

        for(Individual i : population) {
            i.updateGeneMap(amounts);
        }

        int majorityGenes = 0;
        Set<Gene> newShared = new HashSet<>();
        for(Gene g : amounts.keySet()) {
            if(amounts.get(g) > (int)floor(population.size()*0.5)) {
                majorityGenes++;
                newShared.add(g);
            }
        }

        int maintained = 0;
        for(Gene g : shared) {
            if(newShared.contains(g)) {
                maintained++;
            }
        }

        if (maintained > 0.5*shared.size()) {
            generations++;
        } else {
            generations = 0;
        }

        if(currGeneration/100 > minimum) {
            minimum--;
        }

        amounts = new HashMap<>();
        shared = newShared;

        System.out.println("Majority " + majorityGenes + " maintained: " + maintained);
        return majorityGenes >= minimum && generations >= maxGenerationsWithRepeats;
    }


}
