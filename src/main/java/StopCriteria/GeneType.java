package StopCriteria;

public enum GeneType {
    HEIGHT,
    WEAPONS,
    BOOTS,
    HELMETS,
    GAUNTLETS,
    CHESTPIECES;
}
