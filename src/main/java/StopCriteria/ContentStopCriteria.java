package StopCriteria;

import Individuals.Individual;

import java.util.List;

import static java.lang.Math.abs;

public class ContentStopCriteria extends StopCriteria {

    final private double EPSILON = 0.0005;

    final private int maxGenerations;

    private double lastBestFitness = 0;
    private    int     generations = 0;

    public ContentStopCriteria(int maxGenerations){
        super(StopCriteriaType.CONTENT);
        this.maxGenerations = maxGenerations;
    }

    public int getMaxGenerations() {
        return maxGenerations;
    }

    @Override
    public boolean checkIfCriteriaIsMet(List<Individual> population, Individual ret, int currGeneration) {
        if(Double.compare(abs(lastBestFitness - ret.getFitness()), EPSILON) < 0) {
            generations++;
        } else {
            generations = 0;
            lastBestFitness = ret.getFitness();
        }
        return generations >= maxGenerations;
    }
}
