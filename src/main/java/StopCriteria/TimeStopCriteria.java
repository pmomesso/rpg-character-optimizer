package StopCriteria;

import Individuals.Individual;

import java.util.List;

public class TimeStopCriteria extends StopCriteria {
    final private long maxTime;

    final long initTime;

    public TimeStopCriteria(long maxTime){
        super(StopCriteriaType.TIME);
        this.maxTime = maxTime;
        initTime = System.currentTimeMillis();
    }

    public long getTime() {
        return maxTime;
    }

    @Override
    public boolean checkIfCriteriaIsMet(List<Individual> population, Individual ret, int currGeneration) {
        long elapsed = System.currentTimeMillis() - initTime;
        return Long.compare(elapsed, maxTime) >= 0;
    }
}
