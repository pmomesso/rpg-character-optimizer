package StopCriteria;

import Individuals.Individual;

import java.util.List;

public class GenerationsStopCriteria extends StopCriteria {
    final private int maxGenerations;

    public GenerationsStopCriteria(int maxGenerations){
        super(StopCriteriaType.GENERATIONS);
        this.maxGenerations = maxGenerations;
    }

    public int getMaxGenerations() {
        return maxGenerations;
    }

    @Override
    public boolean checkIfCriteriaIsMet(List<Individual> population, Individual ret, int currGeneration) {
        return currGeneration >= maxGenerations;
    }
}
