package StopCriteria;

import Individuals.Individual;

import java.util.List;

public class AcceptableStopCriteria extends StopCriteria {
    final private double minFitness;

    public AcceptableStopCriteria(double minFitness){
        super(StopCriteriaType.ACCEPTABLE);
        this.minFitness = minFitness;
    }

    public double getMinFitness() {
        return minFitness;
    }

    @Override
    public boolean checkIfCriteriaIsMet(List<Individual> population, Individual ret, int currGeneration) {
        return Double.compare(ret.getFitness(), minFitness) >= 0;
    }
}
