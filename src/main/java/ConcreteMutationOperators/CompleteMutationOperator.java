package ConcreteMutationOperators;

import Genes.Gene;
import Operators.MutationOperator;

import static java.lang.Math.random;

public class CompleteMutationOperator implements MutationOperator {
    private double mutationProbability;

    public CompleteMutationOperator(double mutationProbability) {
        this.mutationProbability = mutationProbability;
    }

    @Override
    public boolean mutate(Gene[] chromosome) {
        boolean mutated = false;
        if(willMutate()){
            mutated = true;
            for(int locus = 0; locus < chromosome.length; locus++){
                chromosome[locus] = chromosome[locus].mutate();
            }
        }
        return mutated;
    }

    private boolean willMutate() {
        return Double.compare(random(), mutationProbability) <= 0;
    }

}
