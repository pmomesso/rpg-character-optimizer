package ConcreteMutationOperators;

import Genes.Gene;
import Operators.MutationOperator;

import static java.lang.Math.random;
import static java.lang.StrictMath.floor;

public class SingleGeneMutationOperator implements MutationOperator {

    private double mutationProbability;

    public SingleGeneMutationOperator(double mutationProbability) {
        this.mutationProbability = mutationProbability;
    }

    @Override
    public boolean mutate(Gene[] chromosome) {
        boolean mutated = false;
        if(willMutate()) {
            mutated = true;
            int locus = (int)floor(random()*(chromosome.length));
            chromosome[locus] = chromosome[locus].mutate();
        }
        return mutated;
    }

    private boolean willMutate() {
        return Double.compare(random(), mutationProbability) <= 0;
    }

}
