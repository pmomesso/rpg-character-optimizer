package ConcreteMutationOperators;

import Genes.Gene;
import Operators.MutationOperator;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.floor;
import static java.lang.Math.random;

public class MultigeneLimitedMutationOperator implements MutationOperator {

    private double mutationProbability;

    public MultigeneLimitedMutationOperator(double mutationProbability) {
        this.mutationProbability = mutationProbability;
    }

    @Override
    public boolean mutate(Gene[] chromosome) {
        boolean mutated = false;
        if(willMutate()) {
            mutated = true;
            int genesToMutate = (int)(floor(1 + (random() * chromosome.length)));
            int    considered = 0;
            List<Integer> toMutate = new ArrayList<>(genesToMutate);
            while(considered < genesToMutate) {
                int locus = (int)(floor(random()*(chromosome.length)));
                if(!toMutate.contains(locus)) {
                    toMutate.add(locus);
                    considered++;
                }
            }
            for(int locus : toMutate) {
                chromosome[locus] = chromosome[locus].mutate();
            }
        }
        return mutated;
    }

    private boolean willMutate() {
        return Double.compare(mutationProbability, random()) <= 0;
    }

}
