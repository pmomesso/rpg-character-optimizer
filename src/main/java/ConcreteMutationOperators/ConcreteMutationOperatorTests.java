package ConcreteMutationOperators;

import Genes.Gene;
import Operators.MutationOperator;
import TestingUtils.TestOperators;
import TestingUtils.TestOperators.TestGene;
import org.junit.Assert;
import org.junit.Test;

import static TestingUtils.TestOperators.TEST_DEFAULT_FITNESS;

public class ConcreteMutationOperatorTests {

    @Test
    public void testSingleGeneMutationOperator() {
        MutationOperator mutationOperator = new SingleGeneMutationOperator(1.0);
        TestGene tg = new TestGene();
        Gene[] chromosome = {tg};
        mutationOperator.mutate(chromosome);
        Assert.assertTrue(Double.compare(TEST_DEFAULT_FITNESS/2, tg.fitness) == 0);
    }

}
