package ConcreteMutationOperators;

import Genes.Gene;
import Operators.MutationOperator;

import static java.lang.Math.random;

public class MultigeneUniformMutationOperator implements MutationOperator {

    private double mutationProbability;

    public MultigeneUniformMutationOperator(double mutationProbability) {
        this.mutationProbability = mutationProbability;
    }

    @Override
    public boolean mutate(Gene[] chromosome) {
        boolean mutated = false;
        for(int locus = 0; locus < chromosome.length; locus++) {
            if(willMutate()) {
                mutated = true;
                chromosome[locus] = chromosome[locus].mutate();
            }
        }
        return mutated;
    }

    private boolean willMutate() {
        return Double.compare(random(), mutationProbability) <= 0;
    }

}
