package ConcreteSelectors;

import Individuals.Individual;
import Operators.InverseFitnessFunction;
import Operators.SelectionOperator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import static Individuals.Individual.individualComparator;
import static java.lang.Math.random;

public class RankingSelector implements SelectionOperator {

    private double[] accumulatedRelativeFitness;
    private double   accum;

    private InverseFitnessFunction iff;

    public RankingSelector(InverseFitnessFunction iff) {
        this.iff = iff;
    }

    @Override
    public List<Individual> selectParents(List<Individual> population, int numParents) {
        population.sort(individualComparator);

                             accum = getAccum(population);
        accumulatedRelativeFitness = getAccumulatedRelativeFitness(population);

        List<Individual>      ret = new ArrayList<>(numParents);
                     int selected = 0;
        while (selected < numParents) {
            Individual individual = select(population);
            ret.add(individual);
            selected++;
        }

        return ret;
    }

    private double getAccum(List<Individual> population) {
        double ret = 0;
        for(int i = 0; i < population.size(); i++) {
            ret += iff.inverseFitness(population, i + 1);
        }
        return ret;
    }

    private Individual select(List<Individual> population) {
        if(population.size() == 1) {
            return population.get(0);
        }

        double r = random();

        if(Double.compare(iff.inverseFitness(population, population.size()), r) >= 0) {
            return population.get(0);
        }

        boolean found = false;
            int  curr = 1;
        while(!found && curr < accumulatedRelativeFitness.length) {
            if(Double.compare(r, accumulatedRelativeFitness[curr]) <= 0
            && Double.compare(accumulatedRelativeFitness[curr-1], r) < 0) {
                found = true;
            } else {
                curr++;
            }
        }
        return population.get(curr);
    }

    private double[] getAccumulatedRelativeFitness(List<Individual> population) {
        double[] accumulatedRelativeFitness    = new double[population.size()];
                 accumulatedRelativeFitness[0] = (iff.inverseFitness(population, population.size()));

        BigDecimal accumDecimal = BigDecimal.valueOf(accum);

        for(int i = 1; i < population.size(); i++) {
            accumulatedRelativeFitness[i]  = BigDecimal.valueOf(iff.inverseFitness(population, population.size() - i)).divide(accumDecimal, 5, RoundingMode.HALF_UP).doubleValue();
            accumulatedRelativeFitness[i] += accumulatedRelativeFitness[i-1];
        }

        accumulatedRelativeFitness[population.size() - 1] = 1.0;

        return accumulatedRelativeFitness;
    }

}
