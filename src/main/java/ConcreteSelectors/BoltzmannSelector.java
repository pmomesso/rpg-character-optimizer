package ConcreteSelectors;

import Individuals.DifferentFitnessIndividual;
import Individuals.Individual;
import Operators.SelectionOperator;

import java.util.ArrayList;
import java.util.List;

public class BoltzmannSelector implements SelectionOperator {

    private double  temperature;
    private long    iterations;

    public BoltzmannSelector(double temperature){
        this.temperature = temperature;
        this.iterations  = 1;
    }

    @Override
    public List<Individual> selectParents(List<Individual> population, int numParents) {
        double avgExpVal = avgExpVal(population);
        List<Individual> newFitnessIndividuals = new ArrayList<>(population.size());
        for(Individual individual : population){
            newFitnessIndividuals.add(new DifferentFitnessIndividual(individual, expVal(individual)/avgExpVal));
        }
        SelectionOperator rulleteSelector      = new RuletteSelector();
        List<Individual> newPopulationWrapper  = rulleteSelector.selectParents(newFitnessIndividuals, numParents);
        List<Individual> newPopulation         = new ArrayList<>(newPopulationWrapper.size());
        for(Individual individual : newPopulationWrapper){
            newPopulation.add(((DifferentFitnessIndividual) individual).getIndividual());
        }
        decreaseTemperature();
        return newPopulation;
    }

    private double avgExpVal(List<Individual> population){
        double avg = 0;
        for(Individual individual: population){
            avg += expVal(individual);
        }
        avg = avg/population.size();
        return avg;
    }

    private double expVal(Individual individual){
        return Math.exp(individual.getFitness()/temperature);
    }

    private void decreaseTemperature(){
        temperature = temperature / iterations;
        if(iterations < Long.MAX_VALUE) {
            iterations++;
        }
    }

}
