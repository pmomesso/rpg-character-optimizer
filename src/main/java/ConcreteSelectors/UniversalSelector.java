package ConcreteSelectors;

import Individuals.Individual;
import Operators.SelectionOperator;

import java.util.ArrayList;
import java.util.List;

import static ConcreteSelectors.SelectorUtils.getAccumFitness;
import static ConcreteSelectors.SelectorUtils.getRelativeAccumulatedFitnesses;
import static Individuals.Individual.individualComparator;
import static java.lang.Math.random;

public class UniversalSelector implements SelectionOperator {
    private double   accum;
    private double[] relativeFitnesses;
    private double   r;

    @Override
    public List<Individual> selectParents(List<Individual> population, int numParents) {
        population.sort(individualComparator);

                    accum = getAccumFitness(population);
        relativeFitnesses = getRelativeAccumulatedFitnesses(population, accum);
                        r = random();

        List<Individual> ret = new ArrayList<>(numParents);
        int curr = 0;
        while(ret.size() < numParents) {
            Individual individual = select(population, curr, numParents);
            ret.add(individual);
            curr++;
        }
        return ret;
    }

    private Individual select(List<Individual> population, int current, int numParents) {
        if(population.size() == 1) {
            return population.get(0);
        }

        double currRandom = (r + current)/numParents;

        if(Double.compare(currRandom, relativeFitnesses[0]) <= 0) {
            return population.get(0);
        }

        boolean found = false;
        int  curr = 1;
        while(!found && curr < relativeFitnesses.length) {
            if(Double.compare(currRandom, relativeFitnesses[curr]) <= 0
                    && Double.compare(relativeFitnesses[curr - 1], currRandom) < 0) {
                found = true;
            } else {
                curr++;
            }
        }

        return population.get(curr);
    }

}
