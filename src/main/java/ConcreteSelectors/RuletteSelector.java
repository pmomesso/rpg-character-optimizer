package ConcreteSelectors;

import Individuals.Individual;
import Operators.SelectionOperator;

import java.util.ArrayList;
import java.util.List;

import static ConcreteSelectors.SelectorUtils.getAccumFitness;
import static ConcreteSelectors.SelectorUtils.getRelativeAccumulatedFitnesses;
import static Individuals.Individual.individualComparator;
import static java.lang.Math.random;

public class RuletteSelector implements SelectionOperator {

    private double   accum;
    private double[] relativeFitnesses;

    @Override
    public List<Individual> selectParents(List<Individual> population, int numParents) {
        population.sort(individualComparator);

                    accum = getAccumFitness(population);
        relativeFitnesses = getRelativeAccumulatedFitnesses(population, accum);

        List<Individual> ret = new ArrayList<>(numParents);
        while(ret.size() < numParents) {
            Individual individual = select(population);
            ret.add(individual);
        }
        return ret;
    }

    private Individual select(List<Individual> population) {
        if(population.size() == 1) {
            return population.get(0);
        }

        double r = random();

        if(Double.compare(r, relativeFitnesses[0]) <= 0) {
            return population.get(0);
        }

        boolean found = false;
            int  curr = 1;
        while(!found && curr < relativeFitnesses.length) {
            if(Double.compare(r, relativeFitnesses[curr]) <= 0
            && Double.compare(relativeFitnesses[curr - 1], r) < 0) {
                found = true;
            } else {
                curr++;
            }
        }

        return population.get(curr);
    }

}
