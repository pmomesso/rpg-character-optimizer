package ConcreteSelectors;

import ConcreteFitnessCalculators.ConcreteInverseFitnessFunction;
import Genes.Gene;
import Individuals.Individual;
import TestingUtils.TestOperators.TestCrossOperator;
import TestingUtils.TestOperators.TestFitnessCalculator;
import TestingUtils.TestOperators.TestGene;
import TestingUtils.TestOperators.TestMutationCalculator;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static Individuals.Individual.individualComparator;

public class ConcreteSelectorTests {

    List<Individual> testPopulation = new ArrayList<>();

    private Individual getRandomIndividual() {
        TestGene tg = new TestGene();
         tg.fitness = Math.random();

        Gene[] chromosome = {tg};

        Individual ret = new Individual(chromosome, new TestFitnessCalculator(), new TestMutationCalculator(), new TestCrossOperator());
        return ret;
    }

    private void setTestPopulationToOne() {
        testPopulation.clear();

        TestGene tg = new TestGene();

        Gene[] chromosome = {tg};

        Individual testIndividual = new Individual(chromosome, new TestFitnessCalculator(), new TestMutationCalculator(), new TestCrossOperator());
        testPopulation.add(testIndividual);
    }

    private void setTestPopulationToTwo() {
        testPopulation.clear();

        TestGene tg = new TestGene();
        TestGene tg1 = new TestGene();
        tg1.fitness = 5.0;

        Gene[] chromosome = {tg};
        Gene[] chromosome1 = {tg1};

        Individual testIndividual = new Individual(chromosome, new TestFitnessCalculator(), new TestMutationCalculator(), new TestCrossOperator());
        Individual testIndividual1 = new Individual(chromosome1, new TestFitnessCalculator(), new TestMutationCalculator(), new TestCrossOperator());
        testPopulation.add(testIndividual);
        testPopulation.add(testIndividual1);
    }

    private void setTestPopulation(int populationNumber) {
        testPopulation.clear();
        for(int i = 0; i < populationNumber; i++) {
            testPopulation.add(getRandomIndividual());
        }
    }

    @Test
    public void testEliteSelector() {
        /**Fringe case: population of one individual**/
        setTestPopulationToOne();
        EliteSelector es = new EliteSelector();
        for(int numParents = 1; numParents <= 5; numParents++) {
            List<Individual> selected = es.selectParents(testPopulation, numParents);
            Assert.assertEquals(numParents, selected.size());
            selected.stream().forEach(individual -> Assert.assertEquals(testPopulation.get(0), individual));
        }

        /**Trivial case: population of two individuals, select the fittest as parent**/
        setTestPopulationToTwo();
        List<Individual> selected = es.selectParents(testPopulation, 1);
        Assert.assertEquals(1, selected.size());
        testPopulation.sort(individualComparator);
        Assert.assertEquals(testPopulation.get(1), selected.get(0));

        /**Most common cases: multiple individuals at random, check for selector correctness**/
        for(int numPopulation = 10; numPopulation <= 100; numPopulation++) {
            for(int numParents = 3; numParents <= numPopulation; numParents++) {
                setTestPopulation(numPopulation);
                selected = es.selectParents(testPopulation, numParents);
                Assert.assertEquals(selected.size(), numParents);
                testPopulation.sort(individualComparator);
                for(Individual p : selected) {
                    testPopulation.remove(p);
                    testPopulation.stream().forEach(i -> Assert.assertTrue(individualComparator.compare(p, i) > 0));
                }
            }
        }
    }

    @Test
    public void testRuletteSelector() {
        setTestPopulationToOne();
        RuletteSelector rs = new RuletteSelector();
         /**Fringe case: population of one individual**/
        for(int numParents = 1; numParents <= 5; numParents++) {
            List<Individual> selected = rs.selectParents(testPopulation, numParents);
            Assert.assertEquals(numParents, selected.size());
            selected.stream().forEach(individual -> Assert.assertEquals(testPopulation.get(0), individual));
        }
        /**Trivial case: population of two individuals, select the fittest as parent**/
        setTestPopulationToTwo();
        List<Individual> selected = rs.selectParents(testPopulation, 1);
        Assert.assertEquals(1, selected.size());

        /**Most common cases: multiple individuals at random, check for selector correctness**/
        for(int numPopulation = 10; numPopulation <= 100; numPopulation++) {
            for(int numParents = 3; numParents <= numPopulation; numParents++) {
                setTestPopulation(numPopulation);
                selected = rs.selectParents(testPopulation, numParents);
                Assert.assertEquals(selected.size(), numParents);
            }
        }
    }

    @Test
    public void testUniversalSelector() {
        setTestPopulationToOne();
        UniversalSelector us = new UniversalSelector();
        /**Fringe case: population of one individual**/
        for(int numParents = 1; numParents <= 5; numParents++) {
            List<Individual> selected = us.selectParents(testPopulation, numParents);
            Assert.assertEquals(numParents, selected.size());
            selected.stream().forEach(individual -> Assert.assertEquals(testPopulation.get(0), individual));
        }
        /**Trivial case: population of two individuals, select the fittest as parent**/
        setTestPopulationToTwo();
        List<Individual> selected = us.selectParents(testPopulation, 1);
        Assert.assertEquals(1, selected.size());

        /**Most common cases: multiple individuals at random, check for selector correctness**/
        for(int numPopulation = 10; numPopulation <= 100; numPopulation++) {
            for(int numParents = 3; numParents <= numPopulation; numParents++) {
                setTestPopulation(numPopulation);
                selected = us.selectParents(testPopulation, numParents);
                Assert.assertEquals(selected.size(), numParents);
            }
        }

    }

    @Test
    public void testRankingSelector() {
        setTestPopulationToOne();
        RankingSelector rs = new RankingSelector(new ConcreteInverseFitnessFunction());
        /**Fringe case: population of one individual**/
        for(int numParents = 1; numParents <= 5; numParents++) {
            List<Individual> selected = rs.selectParents(testPopulation, numParents);
            Assert.assertEquals(numParents, selected.size());
            selected.stream().forEach(individual -> Assert.assertEquals(testPopulation.get(0), individual));
        }
        /**Trivial case: population of two individuals, select the fittest as parent**/
        setTestPopulationToTwo();
        List<Individual> selected = rs.selectParents(testPopulation, 1);
        Assert.assertEquals(1, selected.size());

        /**Most common cases: multiple individuals at random, check for selector correctness**/
        for(int numPopulation = 10; numPopulation <= 100; numPopulation++) {
            for(int numParents = 3; numParents <= numPopulation; numParents++) {
                setTestPopulation(numPopulation);
                selected = rs.selectParents(testPopulation, numParents);
                Assert.assertEquals(selected.size(), numParents);
            }
        }

    }

    @Test
    public void testDeterministicTournamentSelector() {
        setTestPopulationToOne();
        DeterministicTournamentSelector dts = new DeterministicTournamentSelector(3);
        /**Fringe case: population of one individual**/
        for(int numParents = 1; numParents <= 5; numParents++) {
            List<Individual> selected = dts.selectParents(testPopulation, numParents);
            Assert.assertEquals(numParents, selected.size());
            selected.stream().forEach(individual -> Assert.assertEquals(testPopulation.get(0), individual));
        }
        /**Trivial case: population of two individuals, select the fittest as parent**/
        setTestPopulationToTwo();
        List<Individual> selected = dts.selectParents(testPopulation, 1);
        Assert.assertEquals(1, selected.size());

        /**Most common cases: multiple individuals at random, check for selector correctness**/
        for(int numPopulation = 10; numPopulation <= 100; numPopulation++) {
            for(int numParents = 3; numParents <= numPopulation; numParents++) {
                setTestPopulation(numPopulation);
                selected = dts.selectParents(testPopulation, numParents);
                Assert.assertEquals(selected.size(), numParents);
            }
        }

    }

}
