package ConcreteSelectors;

import Individuals.Individual;
import Operators.InverseFitnessFunction;

import java.util.List;

public class SelectorUtils {

    public static double[] getRelativeAccumulatedFitnesses(List<Individual> population, double accum) {
        double[] relativeFitnesses    = new double[population.size()];
                 relativeFitnesses[0] = (population.get(0).getFitness())/accum;
        for(int i = 1; i < population.size(); i++) {
            relativeFitnesses[i]  = (population.get(i).getFitness())/accum;
            relativeFitnesses[i] += relativeFitnesses[i - 1];
        }
        return relativeFitnesses;
    }

    public static double getAccumFitness(List<Individual> population) {
        double ret = 0.0;
        for(int i = 0; i < population.size(); i++) {
            ret += population.get(i).getFitness();
        }
        return ret;
    }

}
