package ConcreteSelectors;

import Individuals.Individual;
import Operators.SelectionOperator;

import java.util.ArrayList;
import java.util.List;

import static Individuals.Individual.*;
import static java.lang.Math.floor;
import static java.lang.Math.random;

public class DeterministicTournamentSelector implements SelectionOperator {

    private int participants;

    public DeterministicTournamentSelector(int participants) {
        this.participants = participants;
    }

    @Override
    public List<Individual> selectParents(List<Individual> population, int numParents) {
        List<Individual> ret = new ArrayList<>(numParents);

        int selected = 0;
        while(selected < numParents) {
            List<Individual> tourneyParticipants = getNextParticipants(population);
            tourneyParticipants.sort(individualComparator);
            Individual best = tourneyParticipants.get(participants - 1);
            ret.add(best);
            selected++;
        }

        return ret;
    }

    private List<Individual> getNextParticipants(List<Individual> population) {
        List<Individual> ret = new ArrayList<>(participants);
        for(int i = 0; i < participants; i++) {
            ret.add(population.get((int)floor(random()*population.size())));
        }
        return ret;
    }

}
