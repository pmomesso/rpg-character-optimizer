package ConcreteSelectors;

import Individuals.Individual;
import Operators.SelectionOperator;

import java.util.ArrayList;
import java.util.List;

import static Individuals.Individual.individualComparator;
import static java.lang.Math.ceil;

public class EliteSelector implements SelectionOperator {

    @Override
    public List<Individual> selectParents(List<Individual> population, int numParents) {
        List<Individual> selected = new ArrayList<>(numParents);
        //Sort the list in ascending order of fitness
        population.sort(individualComparator);

        //Select each according to fitness. Break when n(i) <= 0
        int amountSelected = 0;
        int index = population.size() - 1;
        int lastIndex = population.size() - 1;
        int populationSize = population.size();
        do {
            amountSelected = (int)ceil((numParents - (lastIndex - index))/(double)populationSize);
            select(selected, population.get(index), amountSelected);
            index--;
        } while(amountSelected > 0 && index >= 0);
        return selected;
    }

    private void select(List<Individual> selected, Individual individual, int amountSelected) {
        for(int i = 0; i < amountSelected; i++) {
            selected.add(individual);
        }
    }

}
