package ConcreteSelectors;

import Individuals.Individual;
import Operators.SelectionOperator;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.floor;
import static java.lang.Math.random;

public class ProbabilisticTournamentSelector implements SelectionOperator {

    private double threshold;

    public ProbabilisticTournamentSelector(double threshold){
        if(threshold < 0.5 || threshold > 1){
            throw new RuntimeException();
        }
        this.threshold = threshold;
    }

    @Override
    public List<Individual> selectParents(List<Individual> population, int numParents) {
        List<Individual> newPopulation = new ArrayList<>(numParents);
        int selected = 0;
        while(selected < numParents){
            newPopulation.add(chooseFromTwo(population));
            selected++;
        }
        return newPopulation;
    }

    private Individual chooseFromTwo(List<Individual> population){
        int firstIndividualIndex     = (int)floor(random()*(population.size()));
        int secondIndividualIndex    = (int)floor(random()*(population.size()));
        Individual firstIndividual   = population.get(firstIndividualIndex);
        Individual secondIndividual  = population.get(secondIndividualIndex);

        double coin                  = random();
        if(coin < threshold)
            return firstIndividual;
        return secondIndividual;
    }
}
