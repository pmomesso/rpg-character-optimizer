import ConcreteGenes.EquipmentGene;
import Individuals.Individual;
import Operators.CrossOperator;
import Operators.SelectionOperator;
import Parsers.Config.Algos.EngineType;
import Parsers.Config.Algos.SelectionOperatorWrapper;
import Parsers.Config.ConfigFileParser;
import Parsers.Config.Input;
import StopCriteria.StopCriteria;

import java.util.List;

public class Main {

    public static void main(String[] args) {

        ConfigFileParser configFileParser = ConfigFileParser.getInstance("config.json");
        Input input                       = configFileParser.parse();
        if(input == null){
            System.out.println("Bad JSON format");
            System.exit(1);
        }
        EquipmentGene[] weapons     = input.getInventory().getWeapons();
        EquipmentGene[] boots       = input.getInventory().getBoots();
        EquipmentGene[] helmets     = input.getInventory().getHelmets();
        EquipmentGene[] gauntlets   = input.getInventory().getGauntlets();
        EquipmentGene[] chestPieces = input.getInventory().getChestpieces();
        List<Individual> initialPopulation = input.getInitialPopulation();
        StopCriteria sc = input.getGenerationOperators().getStopCriteria();
        SelectionOperator so1 = null;
        SelectionOperator so2 = null;
        SelectionOperator so3 = null;
        SelectionOperator so4 = null;
        CrossOperator co = input.getGenerationOperators().getCrossOperator();
        double a = 0;
        double b = 0;
        so1 = ((SelectionOperatorWrapper)((input.getGenerationOperators().getParentSelectionOperators().toArray()))[0]).getSelectionOperator();
        so2 = ((SelectionOperatorWrapper)((input.getGenerationOperators().getParentSelectionOperators().toArray()))[1]).getSelectionOperator();
        so3 = ((SelectionOperatorWrapper)((input.getGenerationOperators().getNewGenSelectionOperators().toArray()))[0]).getSelectionOperator();
        so4 = ((SelectionOperatorWrapper)((input.getGenerationOperators().getNewGenSelectionOperators().toArray()))[1]).getSelectionOperator();
        a = ((SelectionOperatorWrapper)((input.getGenerationOperators().getParentSelectionOperators().toArray()))[0]).getPonderation();
        b = ((SelectionOperatorWrapper)((input.getGenerationOperators().getNewGenSelectionOperators().toArray()))[0]).getPonderation();
        EngineConfig ec = null;
        if(input.getInitialPopulation() == null) {
            ec = new EngineConfig(so1, so2, so3, so4, input.getGenerationOperators().getMutationOperator(), co, input.getFc(), a, b, sc);
        } else {
            ec = new EngineConfig(so1, so2, so3, so4, co, sc, a, b);
        }
        Engine engine = null;
        if(input.getGenerationOperators().getEngine() == EngineType.FILL_ALL) {
            if(input.getInitialPopulation() != null) {
                engine = new FillAllEngine(ec, initialPopulation, input.getGenerationOperators().getK());
            } else {
                CharacterConfig cc = new CharacterConfig(weapons, boots, helmets, gauntlets, chestPieces);
                engine = new FillAllEngine(ec, cc, input.getSize(), input.getGenerationOperators().getK());
            }
        } else {
            if(input.getInitialPopulation() != null) {
                engine = new FillParentEngine(ec, initialPopulation, input.getGenerationOperators().getK());
            } else {
                CharacterConfig cc = new CharacterConfig(weapons, boots, helmets, gauntlets, chestPieces);
                engine = new FillParentEngine(ec, cc, input.getSize(), input.getGenerationOperators().getK());
            }
        }
        long time = System.currentTimeMillis();
        Individual fittest = engine.iterate();
        System.out.println("Time: " + (System.currentTimeMillis() - time)/1000);
        System.out.println(fittest);
    }

}
