package Individuals;

import Genes.Gene;
import Operators.CrossOperator;
import Operators.FitnessCalculator;
import Operators.MutationOperator;

import java.util.Comparator;
import java.util.Map;

public class Individual {

    public static Comparator<Individual> individualComparator = Comparator.comparingDouble(Individual::getFitness);

    protected            Gene[] chromosome;
    protected FitnessCalculator fc;
    protected  MutationOperator mo;
    protected     CrossOperator co;

    protected double cachedFitness = -1.0;

    public Individual(Gene[] chromosome, FitnessCalculator fc, MutationOperator mo, CrossOperator co) {
        this.chromosome = chromosome;
        this.fc = fc;
        this.mo = mo;
        this.co = co;
    }

    public double getFitness() {
        //if(cachedFitness < 0) {
            cachedFitness = fc.getFitness(chromosome);
        //}
        return cachedFitness;
    }

    public void mutate() {
        if(this.mo.mutate(chromosome)) {
            cachedFitness = fc.getFitness(chromosome);
        }
    }

    public Individual[] cross(Individual individual) {
        return co.crossIndividuals(chromosome, individual.chromosome, fc, mo, co);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(fc.getFitness(chromosome) + " ");
        for(Gene g : chromosome) {
            sb.append(g.toString());
            sb.append(" ");
        }
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if(o == null || !(o instanceof Individual)) return false;
        Individual aux = (Individual)o;
        return aux == this;
    }

    public void updateGeneMap(Map<Gene, Integer> amounts) {
        for(Gene g : chromosome) {
            if(!amounts.containsKey(g)) {
                amounts.put(g, 1);
            } else {
                int q = amounts.get(g);
                amounts.put(g, q + 1);
            }
        }
    }

}