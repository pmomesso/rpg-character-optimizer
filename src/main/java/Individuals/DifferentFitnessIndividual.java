package Individuals;

import Genes.Gene;
import Operators.CrossOperator;
import Operators.FitnessCalculator;
import Operators.MutationOperator;

public class DifferentFitnessIndividual extends Individual{
    private double     fitness;
    private Individual individual;


    public DifferentFitnessIndividual(Individual individual, double fitness) {
        super(individual.chromosome, individual.fc, individual.mo, individual.co);
        this.individual = individual;
        this.fitness = fitness;
    }

    @Override
    public double getFitness() {
        return fitness;
    }

    public void mutate() {
        this.individual.mo.mutate(chromosome);
    }

    public Individual[] cross(Individual individual) {
        return this.individual.co.crossIndividuals(this.individual.chromosome, individual.chromosome, this.individual.fc, this.individual.mo, this.individual.co);
    }

    public Individual getIndividual() {
        return individual;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.individual.fc.getFitness(this.individual.chromosome));
        return sb.toString();
    }
}
