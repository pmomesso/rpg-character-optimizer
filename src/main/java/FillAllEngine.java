import Individuals.Individual;

import java.util.ArrayList;
import java.util.List;

public class FillAllEngine extends Engine {

    public FillAllEngine(EngineConfig engineConfig, CharacterConfig characterConfig, int populationSize, int numParents) {
        super(engineConfig, characterConfig, populationSize, numParents);
    }

    public FillAllEngine(EngineConfig engineConfig, List<Individual> initialPopulation, int numParents) {
        super(engineConfig, initialPopulation, numParents);
    }

    @Override
    protected double setPopulation(List<Individual> parents, List<Individual> children) {
        List<Individual> newPopulation = new ArrayList<>(population.size());
        double bestFitness = 0.0;
        if(numParents >= population.size()) {
            newPopulation.addAll(so3.selectParents(children, selection1));
            newPopulation.addAll(so4.selectParents(children, selection2));
        } else {
            parents.addAll(children);
            newPopulation.addAll(so3.selectParents(parents, selection1));
            newPopulation.addAll(so4.selectParents(parents, selection2));
        }

        population = newPopulation;
        return bestFitness;
    }

}
