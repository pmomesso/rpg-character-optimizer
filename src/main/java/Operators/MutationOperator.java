package Operators;

import Genes.Gene;

public interface MutationOperator {

    boolean mutate(Gene[] chromosome);

}
