package Operators;

import Individuals.Individual;

import java.util.List;

public interface InverseFitnessFunction {

    double inverseFitness(List<Individual> population, int ranking);

}
