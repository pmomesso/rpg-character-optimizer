package Operators;

import Individuals.Individual;

import java.util.List;

public interface SelectionOperator {

    List<Individual> selectParents(List<Individual> population, int numParents);

}
