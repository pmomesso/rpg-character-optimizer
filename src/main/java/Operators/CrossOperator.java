package Operators;

import Genes.Gene;
import Individuals.Individual;

public interface CrossOperator {

    Individual[] crossIndividuals(Gene[] chromosomes1, Gene[] chromosomes2, FitnessCalculator fc, MutationOperator mo, CrossOperator co);

}
