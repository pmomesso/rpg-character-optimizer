package Operators;

import Genes.Gene;

public interface FitnessCalculator {

    double getFitness(Gene[] chromosome);

}
