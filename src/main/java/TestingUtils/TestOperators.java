package TestingUtils;

import Genes.Gene;
import Individuals.Individual;
import Operators.CrossOperator;
import Operators.FitnessCalculator;
import Operators.MutationOperator;

public class TestOperators {

    public static final double TEST_DEFAULT_FITNESS = 1.0;

    public static class TestGene extends Gene {
        /**Public for testing purposes**/public double fitness = TEST_DEFAULT_FITNESS;
        @Override
        public Gene mutate() {
            fitness /= 2;
            return this;
        }

        @Override
        public Gene copy() {
            TestGene tg = new TestGene();
            tg.fitness = this.fitness;
            return tg;
        }
    }

    public static class TestFitnessCalculator implements FitnessCalculator {

        @Override
        public double getFitness(Gene[] chromosome) {
            TestGene tg = ((TestGene)chromosome[0]);
            return tg.fitness;
        }
    }

    public static class TestMutationCalculator implements MutationOperator {

        @Override
        public boolean mutate(Gene[] chromosome) {
            /**Empty for testing**/
            return false;
        }
    }

    public static class TestCrossOperator implements CrossOperator {

        @Override
        public Individual[] crossIndividuals(Gene[] chromosomes1, Gene[] chromosomes2, FitnessCalculator fc, MutationOperator mo, CrossOperator co) {
            /**Empty for testing**/
            return null;
        }

    }

}
