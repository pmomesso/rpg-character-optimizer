package ConcreteGenes;

import Genes.Gene;

import static java.lang.Math.floor;
import static java.lang.Math.random;

public class EquipmentGene extends Gene {

    private    int id;
    private double strength, agility, stealth, resistance, vitality;
    private EquipmentGene[] genePool;

    public EquipmentGene(int id, double strength, double agility, double stealth, double resistance, double vitality) {
        this.id         = id;
        this.strength   = strength;
        this.agility    = agility;
        this.stealth    = stealth;
        this.resistance = resistance;
        this.vitality   = vitality;
    }

    public double getStrength() {
        return strength;
    }

    public double getAgility() {
        return agility;
    }

    public double getStealth() {
        return stealth;
    }

    public double getResistance() {
        return resistance;
    }

    public double getVitality() {
        return vitality;
    }

    @Override
    public Gene mutate() {
        int index = (int)floor(random()*(genePool.length-1));
        return genePool[index];
    }

    @Override
    public Gene copy() {
        EquipmentGene eg = new EquipmentGene(id, strength, agility, stealth, resistance, vitality);
        eg.genePool = genePool;
        return eg;
    }

    @Override
    public String toString() {
        return String.valueOf(id);
    }

    public void setPossibleValues(EquipmentGene[] genePool) {
        this.genePool = genePool;
    }

}