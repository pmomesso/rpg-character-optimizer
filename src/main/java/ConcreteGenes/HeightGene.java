package ConcreteGenes;

import Genes.Gene;

import static java.lang.Math.random;

public class HeightGene extends Gene {

    private static final double   MIN_HEIGHT = 1.3;
    private static final double   MAX_HEIGHT = 2.0;
    private static final double DELTA_HEIGHT = MAX_HEIGHT - MIN_HEIGHT;

    private double height;

    public HeightGene(double height) {
        this.height = height;
    }

    @Override
    public Gene mutate() {
        height = getRandomHeight();
        return this;
    }

    private double getRandomHeight() {
        return random()*DELTA_HEIGHT + MIN_HEIGHT;
    }

    @Override
    public Gene copy() {
        HeightGene hg = new HeightGene(height);
        return hg;
    }

    public double getHeight() {
        return height;
    }

    @Override
    public String toString() {
        return String.valueOf(height);
    }

}
