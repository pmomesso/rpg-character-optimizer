import Individuals.Individual;
import Operators.SelectionOperator;
import StopCriteria.StopCriteria;

import java.util.ArrayList;
import java.util.List;

import static Individuals.Individual.individualComparator;
import static java.lang.Math.floor;

public abstract class Engine {

    protected    List<Individual> population;
    protected IndividualGenerator generator;

    protected StopCriteria sc;

    protected SelectionOperator so1;
    protected SelectionOperator so2;
    protected SelectionOperator so3;
    protected SelectionOperator so4;

    protected int numParents;

    protected int numParents1, numParents2;
    protected int  selection1,  selection2;

    public Engine(EngineConfig engineConfig, CharacterConfig characterConfig, int populationSize, int numParents) {
        so1 = engineConfig.getSo1();
        so2 = engineConfig.getSo2();
        so3 = engineConfig.getSo3();
        so4 = engineConfig.getSo4();

        sc = engineConfig.getStopCriteria();

        double percentageA = engineConfig.getPercentageA();
        numParents1 = (int)floor(percentageA*numParents);
        numParents2 = numParents - numParents1;

        double percentageB = engineConfig.getPercentageB();
        selection1 = (int)floor(percentageB*populationSize);
        selection2 = populationSize - selection1;

        generator = new IndividualGenerator(engineConfig.getFc()
                    , engineConfig.getMo()
                    , engineConfig.getCo()
                    , characterConfig);
        population = generator.generatePopulation(populationSize);

        this.numParents = numParents;
    }

    public Engine(EngineConfig engineConfig, List<Individual> initialPopulation, int numParents) {
        so1 = engineConfig.getSo1();
        so2 = engineConfig.getSo2();
        so3 = engineConfig.getSo3();
        so4 = engineConfig.getSo4();

        sc = engineConfig.getStopCriteria();

        double percentageA = engineConfig.getPercentageA();
        numParents1 = (int)floor(percentageA*numParents);
        numParents2 = numParents - numParents1;

        double percentageB = engineConfig.getPercentageB();
        selection1 = (int)floor(percentageB*initialPopulation.size());
        selection2 = initialPopulation.size() - selection1;

        this.numParents = numParents;
    }


    private void cycle() {
        //Select parents
        List<Individual> parents = so1.selectParents(population, numParents1);
        parents.addAll(so2.selectParents(population, numParents2));

        //Cross individuals
        List<Individual> children = crossParents(parents);

        setPopulation(parents, children);
    }

    public Individual iterate() {
        int currGeneration = 0;
        Individual ret = null;
         do {
            printState(currGeneration);
            cycle();
            ret = population.stream().max(individualComparator).get();
            currGeneration++;
        } while(!sc.checkIfCriteriaIsMet(population, ret, currGeneration));
        return ret;
    }

    protected abstract double setPopulation(List<Individual> parents, List<Individual> children);

    protected List<Individual> crossParents(List<Individual> parents) {
        int toGenerate = numParents;
        if(numParents > population.size()) {
            toGenerate = population.size();
        }
        List<Individual> children = new ArrayList<>();

        int generated = 0;
        int      curr = 0;
        while(generated < toGenerate) {
            Individual[] offSpring = null;
            if(curr >= parents.size() - 1) {
                //Cross this individual with first parent
                offSpring = parents.get(parents.size() - 1).cross(parents.get(0));
            } else {
                //Cross this individual with the next
                offSpring = parents.get(curr).cross(parents.get(curr + 1));
            }
            //Probabilistically mutate
            for(Individual child : offSpring) {
                child.mutate();
                if(children.size() < toGenerate) {
                    children.add(child);
                    generated++;
                }
            }

            curr += 2;
        }
        return children;
    }

    private void printState(long currGeneration) {
        System.out.println("Current generation: " + currGeneration);
        for(Individual individual : population) {
            System.out.println(individual);
        }
        System.out.println();
    }


}
