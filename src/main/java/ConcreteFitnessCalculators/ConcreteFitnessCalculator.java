package ConcreteFitnessCalculators;

import Genes.Gene;
import Operators.FitnessCalculator;

public class ConcreteFitnessCalculator implements FitnessCalculator {

    private double ponderation1;
    private double ponderation2;

    public ConcreteFitnessCalculator(double ponderation1, double ponderation2) {
        this.ponderation1 = ponderation1;
        this.ponderation2 = ponderation2;
    }

    @Override
    public double getFitness(Gene[] chromosome) {
        return ponderation1*ConcreteFitnessUtils.getAttack(chromosome) + ponderation2*ConcreteFitnessUtils.getDefense(chromosome);
    }

}
