package ConcreteFitnessCalculators;

import ConcreteGenes.EquipmentGene;
import ConcreteGenes.HeightGene;
import Genes.Gene;

import static java.lang.Math.pow;
import static java.lang.Math.tanh;

public class ConcreteFitnessUtils {

    public static double getAttack(Gene[] chromosome) {
        double  agility = getAgility(chromosome);
        double  stealth = getStealth(chromosome);
        double strength = getStrength(chromosome);
        double modifier = getAttackModifier(chromosome);
        return (agility + stealth)*strength*modifier;
    }

    public static double getDefense(Gene[] chromosome) {
        double resistance = getResistance(chromosome);
        double    stealth = getStealth(chromosome);
        double   vitality = getVitality(chromosome);
        double   modifier = getDefenseModifier(chromosome);
        return (resistance + stealth)*vitality*modifier;
    }

    private static double getAttackModifier(Gene[] chromosome) {
        double h = ((HeightGene)chromosome[0]).getHeight();
        return 0.7 - pow((3.0*h - 5), 4) + pow((3*h - 5), 2) + h/4.0;
    }

    private static double getDefenseModifier(Gene[] chromosome) {
        double h = ((HeightGene)chromosome[0]).getHeight();
        return 1.9 + pow((2.5*h - 4.16), 4) - pow((2.5*h - 4.16), 2) - 3.0*h/10.0;
    }

    private static double getStrength(Gene[] chromosome) {
        double acum = 0;
        for(int i = 1; i < chromosome.length; i++) {
            acum += ((EquipmentGene)chromosome[i]).getStrength();
        }
        return 100*tanh(0.01*acum);
    }

    private static double getStealth(Gene[] chromosome) {
        double acum = 0;
        for(int i = 1; i < chromosome.length; i++) {
            acum += ((EquipmentGene)chromosome[i]).getStealth();
        }
        return 0.6*tanh(0.01*acum);
    }

    private static double getAgility(Gene[] chromosome) {
        double acum = 0;
        for(int i = 1; i < chromosome.length; i++) {
            acum += ((EquipmentGene)chromosome[i]).getAgility();
        }
        return tanh(0.01*acum);
    }

    private static double getVitality(Gene[] chromosome) {
        double acum = 0;
        for(int i = 1; i < chromosome.length; i++) {
            acum += ((EquipmentGene)chromosome[i]).getVitality();
        }
        return 100*tanh(0.01*acum);
    }

    private static double getResistance(Gene[] chromosome) {
        double acum = 0;
        for(int i = 1; i < chromosome.length; i++) {
            acum += ((EquipmentGene)chromosome[i]).getResistance();
        }
        return tanh(0.01*acum);
    }



}
