package ConcreteFitnessCalculators;

import Individuals.Individual;
import Operators.InverseFitnessFunction;

import java.util.List;

public class ConcreteInverseFitnessFunction implements InverseFitnessFunction {

    @Override
    public double inverseFitness(List<Individual> population, int ranking) {
        return ((double)population.size() - (double)ranking)/population.size();
    }

}
