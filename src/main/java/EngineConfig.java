import Operators.CrossOperator;
import Operators.FitnessCalculator;
import Operators.MutationOperator;
import Operators.SelectionOperator;
import StopCriteria.StopCriteria;

public class EngineConfig {

    private SelectionOperator so1;
    private SelectionOperator so2;
    private SelectionOperator so3;
    private SelectionOperator so4;
    private MutationOperator mo;
    private CrossOperator co;
    private FitnessCalculator fc;

    private final      StopCriteria stopCriteria;

    private final            double percentageA;
    private final            double percentageB;

    public EngineConfig(SelectionOperator so1, SelectionOperator so2, SelectionOperator so3, SelectionOperator so4, MutationOperator mo, CrossOperator co, FitnessCalculator fc
            , double percentageA, double percentageB, StopCriteria stopCriteria) {
        this.so1 = so1;
        this.so2 = so2;
        this.so3 = so3;
        this.so4 = so4;
        this.mo = mo;
        this.co = co;
        this.fc = fc;
        this.stopCriteria = stopCriteria;
        this.percentageA = percentageA;
        this.percentageB = percentageB;
    }

    public EngineConfig(SelectionOperator so1, SelectionOperator so2, SelectionOperator so3, SelectionOperator so4, CrossOperator co, StopCriteria stopCriteria, double percentageA, double percentageB) {
        this.so1 = so1;
        this.so2 = so2;
        this.so3 = so3;
        this.so4 = so4;
        this.co = co;
        this.stopCriteria = stopCriteria;
        this.percentageA = percentageA;
        this.percentageB = percentageB;
    }

    public SelectionOperator getSo1() {
        return so1;
    }

    public SelectionOperator getSo2() {
        return so2;
    }

    public SelectionOperator getSo3() {
        return so3;
    }

    public SelectionOperator getSo4() {
        return so4;
    }

    public StopCriteria getStopCriteria() {
        return stopCriteria;
    }

    public double getPercentageA() {
        return percentageA;
    }

    public double getPercentageB() {
        return percentageB;
    }

    public MutationOperator getMo() {
        return mo;
    }

    public CrossOperator getCo() {
        return co;
    }

    public FitnessCalculator getFc() {
        return fc;
    }
}
