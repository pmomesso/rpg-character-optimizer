import Individuals.Individual;

import java.util.List;

public class FillParentEngine extends Engine {

    public FillParentEngine(EngineConfig engineConfig, CharacterConfig characterConfig,
                            int populationSize, int numParents) {
        super(engineConfig, characterConfig, populationSize, numParents);
    }

    public FillParentEngine(EngineConfig engineConfig, List<Individual> initialPopulation, int numParents) {
        super(engineConfig, initialPopulation, numParents);
    }

    @Override
    protected double setPopulation(List<Individual> parents, List<Individual> children) {
        /**Fill-Parent: if the amount of children is less than the amount of individuals in population
         * then the new population is A U children, where #A = populationSize - numChildren. Else the new population is composed of just
         * children
         ***/
        if(numParents <= population.size()) {
            addEnoughToParents(parents);
            parents.addAll(children);
            population = parents;
        } else {
            population = children;
        }
        return 0.0;
    }

    private void addEnoughToParents(List<Individual> parents) {
        parents.addAll(so3.selectParents(parents, selection1));
    }

}
