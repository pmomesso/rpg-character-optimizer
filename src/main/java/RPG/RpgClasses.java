package RPG;

public enum RpgClasses {

    WARRIOR("warrior", 0.6, 0.6)
    , ARCHER("archer", 0.9, 0.1)
    , TANK("tank", 0.3, 0.8)
    , SPY("spy", 0.8, 0.3);

    final private String name;
    final private double ponderation;
    final private double ponderation1;

    RpgClasses(String name, double ponderation, double ponderation1) {
        this.name = name;
        this.ponderation = ponderation;
        this.ponderation1 = ponderation1;
    }

    public static RpgClasses getRpgClasses(String name){
        for(RpgClasses rpgClasses : RpgClasses.values()){
            if(name.equals(rpgClasses.name))
                return rpgClasses;
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public double getPonderation() {
        return ponderation;
    }

    public double getPonderation1() {
        return ponderation1;
    }
}
