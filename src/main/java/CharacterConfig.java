import ConcreteGenes.EquipmentGene;

public class CharacterConfig {

    final private EquipmentGene[] weapons;
    final private EquipmentGene[] boots;
    final private EquipmentGene[] helmets;
    final private EquipmentGene[] gauntlets;
    final private EquipmentGene[] chestPieces;

    public CharacterConfig(EquipmentGene[] weapons, EquipmentGene[] boots, EquipmentGene[] helmets, EquipmentGene[] gauntlets, EquipmentGene[] chestPieces){
        this.weapons        = weapons;
        this.boots          = boots;
        this.helmets        = helmets;
        this.gauntlets      = gauntlets;
        this.chestPieces    = chestPieces;
    }

    public EquipmentGene[] getWeapons() {
        return weapons;
    }

    public EquipmentGene[] getBoots() {
        return boots;
    }

    public EquipmentGene[] getHelmets() {
        return helmets;
    }

    public EquipmentGene[] getGauntlets() {
        return gauntlets;
    }

    public EquipmentGene[] getChestPieces() {
        return chestPieces;
    }
}
