package Parsers;

public interface Parser<T> {
    T parse();
}
