package Parsers.Config;

public class ConfigParserUtils {
    public static int howManyTrue(boolean... booleans){
        int trues = 0;
        for(boolean bool : booleans){
            if(bool){
                trues++;
            }
        }
        return trues;
    }
}
