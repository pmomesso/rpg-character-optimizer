package Parsers.Config;

import ConcreteFitnessCalculators.ConcreteFitnessCalculator;
import ConcreteGenes.HeightGene;
import Genes.Gene;
import Individuals.Individual;
import Operators.CrossOperator;
import Operators.MutationOperator;
import Parsers.Config.Equipment.Inventory;
import Parsers.Parser;
import RPG.RpgClasses;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class InitialPopulationParser implements Parser<InitialPopulationParserResult> {


    final private static String            INITIAL_POPULATION_KEYWORD  = "characters";

    final private static String            CHARACTER_CLASS_KEYWORD      = "class";
    final private static String            CHARACTER_QUIRKS_KEYWORD     = "quirks";
    final private static String            CHARACTER_HEIGHT_KEYWORD     = "height";
    final private static String            CHARACTER_EQUIPMENT_KEYWORD  = "equipment";

    final private static String            CHARACTER_WEAPON_KEYWORD     = "weapon";
    final private static String            CHARACTER_HELMET_KEYWORD     = "helmet";
    final private static String            CHARACTER_CHESTPIECE_KEYWORD = "chestpiece";
    final private static String            CHARACTER_GAUNTLET_KEYWORD   = "gauntlet";
    final private static String            CHARACTER_BOOT_KEYWORD       = "boot";

    private static InitialPopulationParser initialPopulationParser = null;
    private JSONObject                     config                  = null;
    private Inventory                      inventory               = null;
    private MutationOperator               mutationOperator        = null;
    private CrossOperator                  crossOperator           = null;

    private InitialPopulationParser(JSONObject config, Inventory inventory){
        this.config     = config;
        this.inventory  = inventory;
    }

    public static InitialPopulationParser getInstance(JSONObject config, Inventory inventory
            , MutationOperator mutationOperator
            , CrossOperator crossOperator) {
        if(initialPopulationParser == null)
            initialPopulationParser = new InitialPopulationParser(config, inventory);
        if(!initialPopulationParser.config.equals(config))
            initialPopulationParser.config = config;
        if (initialPopulationParser.inventory == null || !initialPopulationParser.inventory.equals(inventory))
            initialPopulationParser.inventory = inventory;
        if(initialPopulationParser.mutationOperator == null || !initialPopulationParser.mutationOperator.equals(mutationOperator))
            initialPopulationParser.mutationOperator = mutationOperator;
        if(initialPopulationParser.crossOperator == null || !initialPopulationParser.crossOperator.equals(crossOperator))
            initialPopulationParser.crossOperator = crossOperator;
        return initialPopulationParser;
    }

    @Override
    public InitialPopulationParserResult parse() {
        List<Individual> initialPopulation;
        ConcreteFitnessCalculator concreteFitnessCalculator;
        InitialPopulationParserResult ret = new InitialPopulationParserResult();
        try {
            JSONObject characters       = (JSONObject) config.get(INITIAL_POPULATION_KEYWORD);
            String     characterClass   = (String)     characters.get(CHARACTER_CLASS_KEYWORD);
            RpgClasses rpgClass         = RpgClasses.getRpgClasses(characterClass);
            boolean    randomPopulation = (boolean) characters.get("random");
            if(randomPopulation) {
                int size = (int) ((Long)characters.get("randomPopulationSize")).intValue();
                ret.setRandom(true);
                ret.setSize(size);
                ret.setFc(new ConcreteFitnessCalculator(rpgClass.getPonderation(), rpgClass.getPonderation1()));
                return ret;
            }
            ret.setRandom(false);
            if(rpgClass == null)
                return null;
            JSONArray  characterQuirks  = (JSONArray)  characters.get(CHARACTER_QUIRKS_KEYWORD);
            initialPopulation = new ArrayList<>(characterQuirks.size());
            for(Object object : characterQuirks) {
                JSONObject characterQuirk      = (JSONObject) object;
                double     characterHeight     = (double)     characterQuirk.get(CHARACTER_HEIGHT_KEYWORD);
                JSONObject characterEquipment  = (JSONObject) characterQuirk.get(CHARACTER_EQUIPMENT_KEYWORD);
                int        characterWeapon     = ((Long)      characterEquipment.get(CHARACTER_WEAPON_KEYWORD)).intValue();
                int        characterHelmet     = ((Long)      characterEquipment.get(CHARACTER_HELMET_KEYWORD)).intValue();
                int        characterChestPiece = ((Long)      characterEquipment.get(CHARACTER_CHESTPIECE_KEYWORD)).intValue();
                int        characterGauntlet   = ((Long)      characterEquipment.get(CHARACTER_GAUNTLET_KEYWORD)).intValue();
                int        characterBoot       = ((Long)      characterEquipment.get(CHARACTER_BOOT_KEYWORD)).intValue();
                Gene[] genes = new Gene[6];
                genes[0] = new HeightGene(characterHeight);
                genes[1] = inventory.getWeapons()[characterWeapon];
                genes[2] = inventory.getBoots()[characterBoot];
                genes[3] = inventory.getHelmets()[characterHelmet];
                genes[4] = inventory.getGauntlets()[characterGauntlet];
                genes[5] = inventory.getChestpieces()[characterChestPiece];
                initialPopulation.add(new Individual(genes, new ConcreteFitnessCalculator(rpgClass.getPonderation(), rpgClass.getPonderation1()), mutationOperator, crossOperator));
            }
            ret.setInitialPopulation(initialPopulation);
            return ret;
        } catch (Exception e){
            return null;
        }
    }
}