package Parsers.Config;

import Individuals.Individual;
import Operators.FitnessCalculator;

import java.util.List;

public class InitialPopulationParserResult {

    private boolean isRandom;
    private int size;
    private List<Individual> initialPopulation;
    private FitnessCalculator fc;

    public boolean isRandom() {
        return isRandom;
    }

    public void setRandom(boolean random) {
        isRandom = random;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public List<Individual> getInitialPopulation() {
        return initialPopulation;
    }

    public void setInitialPopulation(List<Individual> initialPopulation) {
        this.initialPopulation = initialPopulation;
    }

    public FitnessCalculator getFc() {
        return fc;
    }

    public void setFc(FitnessCalculator fc) {
        this.fc = fc;
    }
}
