package Parsers.Config;

import Operators.*;
import Parsers.Config.Algos.EngineType;
import Parsers.Config.Algos.SelectionOperatorWrapper;
import StopCriteria.StopCriteria;

import java.util.Collection;

public class GenerationOperators {
    final private CrossOperator                        crossOperator;
    final private MutationOperator                     mutationOperator;
    final private Collection<SelectionOperatorWrapper> parentSelectionOperators;
    final private Collection<SelectionOperatorWrapper> newGenSelectionOperators;
    final private EngineType                           engine;
    final private StopCriteria                         stopCriteria;
    final private int                                  K;

    public GenerationOperators(EngineType engine, CrossOperator crossOperator
            , MutationOperator mutationOperator
            , Collection<SelectionOperatorWrapper> parentSelectionOperators
            , Collection<SelectionOperatorWrapper> newGenSelectionOperators
            , StopCriteria stopCriteria, int K){
        this.crossOperator            = crossOperator;
        this.mutationOperator         = mutationOperator;
        this.parentSelectionOperators = parentSelectionOperators;
        this.newGenSelectionOperators = newGenSelectionOperators;
        this.engine                   = engine;
        this.stopCriteria             = stopCriteria;
        this.K = K;
    }

    public CrossOperator getCrossOperator() {
        return crossOperator;
    }

    public MutationOperator getMutationOperator() {
        return mutationOperator;
    }

    public Collection<SelectionOperatorWrapper> getParentSelectionOperators() {
        return parentSelectionOperators;
    }

    public Collection<SelectionOperatorWrapper> getNewGenSelectionOperators() {
        return newGenSelectionOperators;
    }

    public EngineType getEngine() {
        return engine;
    }

    public StopCriteria getStopCriteria() {
        return stopCriteria;
    }

    public int getK() {
        return K;
    }
}
