package Parsers.Config.Algos;

import Parsers.Parser;
import org.json.simple.JSONObject;

public class ImplementationParser implements Parser<EngineType> {

    final private static String         FILL_ALL_KEYWORD     = "fill-all";
    final private static String         FILL_PARENT_KEYWORD  = "fill-parent";

    private static ImplementationParser implementationParser = null;
    private JSONObject                  jsonObject           = null;

    public ImplementationParser(JSONObject jsonObject){
        this.jsonObject      = jsonObject;
    }

    public static ImplementationParser getInstance(JSONObject jsonObject){
        if(implementationParser == null)
            implementationParser = new ImplementationParser(jsonObject);
        if(!implementationParser.jsonObject.equals(jsonObject))
            implementationParser.jsonObject = jsonObject;
        return implementationParser;
    }

    @Override
    public EngineType parse() {
        try {
            boolean fillAll    = (boolean) jsonObject.get(FILL_ALL_KEYWORD);
            boolean fillParent = (boolean) jsonObject.get(FILL_PARENT_KEYWORD);
            if(fillAll)
                return EngineType.FILL_ALL;
            if(fillParent)
                return EngineType.FILL_PARENT;
            return null;
        } catch (Exception e) {
            return null;
        }
    }
}
