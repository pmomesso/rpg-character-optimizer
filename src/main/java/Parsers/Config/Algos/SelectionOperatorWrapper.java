package Parsers.Config.Algos;

import Operators.SelectionOperator;

public class SelectionOperatorWrapper {
    final private SelectionOperator selectionOperator;
    final private double            ponderation;

    public SelectionOperatorWrapper(SelectionOperator selectionOperator, double ponderation){
        this.selectionOperator = selectionOperator;
        this.ponderation       = ponderation;
    }

    public SelectionOperator getSelectionOperator() {
        return selectionOperator;
    }

    public double getPonderation() {
        return ponderation;
    }
}
