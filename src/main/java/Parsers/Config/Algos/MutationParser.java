package Parsers.Config.Algos;

import ConcreteMutationOperators.CompleteMutationOperator;
import ConcreteMutationOperators.MultigeneLimitedMutationOperator;
import ConcreteMutationOperators.MultigeneUniformMutationOperator;
import ConcreteMutationOperators.SingleGeneMutationOperator;
import Operators.MutationOperator;
import Parsers.Config.ConfigParserUtils;
import Parsers.Parser;
import org.json.simple.JSONObject;

public class MutationParser implements Parser<MutationOperator> {

    final private static String ACTIVE_OPERATOR_KEYWORD      = "active";
    final private static String MUTATION_PROBABILITY_KEYWORD = "mutation-probability";
    final private static String SINGLE_GENE_KEYWORD          = "single-gene";
    final private static String LIMITED_MULTIGENE_KEYWORD    = "limited-multigene";
    final private static String UNIFORM_MULTIGENE_KEYWORD    = "uniform-multigene";
    final private static String COMPLETE_KEYWORD             = "complete";

    private static MutationParser mutationParser = null;
    private JSONObject            jsonObject     = null;

    private MutationParser(JSONObject jsonObject){
        this.jsonObject = jsonObject;
    }

    public static MutationParser getInstance(JSONObject jsonObject){
        if(mutationParser == null)
            mutationParser = new MutationParser(jsonObject);
        if(!mutationParser.jsonObject.equals(jsonObject))
            mutationParser.jsonObject = jsonObject;
        return mutationParser;
    }

    @Override
    public MutationOperator parse() {
        try {
            JSONObject singleGene                     = (JSONObject) jsonObject.get(SINGLE_GENE_KEYWORD);
            JSONObject limitedMultigene               = (JSONObject) jsonObject.get(LIMITED_MULTIGENE_KEYWORD);
            JSONObject uniformMultigene               = (JSONObject) jsonObject.get(UNIFORM_MULTIGENE_KEYWORD);
            JSONObject complete                       = (JSONObject) jsonObject.get(COMPLETE_KEYWORD);

            boolean    isSingleGene                   = (boolean)    singleGene.get(ACTIVE_OPERATOR_KEYWORD);
            boolean    isLimitedMultigene             = (boolean)    limitedMultigene.get(ACTIVE_OPERATOR_KEYWORD);
            boolean    isUniformMultigene             = (boolean)    uniformMultigene.get(ACTIVE_OPERATOR_KEYWORD);
            boolean    isComplete                     = (boolean)    complete.get(ACTIVE_OPERATOR_KEYWORD);

            if(ConfigParserUtils.howManyTrue(isSingleGene, isLimitedMultigene, isUniformMultigene, isComplete) != 1)
                return null;

            if(isSingleGene){
                double     singleGeneMutationProbability  = (double)     singleGene.get(MUTATION_PROBABILITY_KEYWORD);
                return new SingleGeneMutationOperator(singleGeneMutationProbability);
            }
            if(isLimitedMultigene){
                double     limitedGeneMutationProbability = (double)     limitedMultigene.get(MUTATION_PROBABILITY_KEYWORD);
                return new MultigeneLimitedMutationOperator(limitedGeneMutationProbability);
            }
            if(isUniformMultigene){
                double     uniformGeneMutationProbability = (double)     uniformMultigene.get(MUTATION_PROBABILITY_KEYWORD);
                return new MultigeneUniformMutationOperator(uniformGeneMutationProbability);
            }
            if(isComplete){
                double     completeMutationProbability    = (double)     complete.get(MUTATION_PROBABILITY_KEYWORD);
                return new CompleteMutationOperator(completeMutationProbability);
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }
}
