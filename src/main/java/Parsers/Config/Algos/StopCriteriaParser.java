package Parsers.Config.Algos;

import Parsers.Config.ConfigParserUtils;
import Parsers.Parser;
import StopCriteria.StopCriteria;
import StopCriteria.TimeStopCriteria;
import StopCriteria.GenerationsStopCriteria;
import StopCriteria.AcceptableStopCriteria;
import StopCriteria.StopCriteriaType;
import StopCriteria.GeneType;
import StopCriteria.StructureStopCriteria;
import StopCriteria.ContentStopCriteria;
import org.json.simple.JSONObject;

import java.util.HashSet;
import java.util.Set;

public class StopCriteriaParser implements Parser<StopCriteria> {

    final private static String       ACTIVE_KEYWORD     = "active";

    private static StopCriteriaParser stopCriteriaParser = null;
    private JSONObject                jsonObject         = null;

    private StopCriteriaParser(JSONObject jsonObject){
        this.jsonObject = jsonObject;
    }

    public static StopCriteriaParser getInstance(JSONObject jsonObject){
        if(stopCriteriaParser == null)
            stopCriteriaParser = new StopCriteriaParser(jsonObject);
        if(!stopCriteriaParser.jsonObject.equals(jsonObject))
            stopCriteriaParser.jsonObject = jsonObject;
        return stopCriteriaParser;
    }

    @Override
    public StopCriteria parse() {
        try {

            JSONObject time          = (JSONObject) jsonObject.get(StopCriteriaType.TIME.getReference());
            JSONObject generations   = (JSONObject) jsonObject.get(StopCriteriaType.GENERATIONS.getReference());
            JSONObject acceptable    = (JSONObject) jsonObject.get(StopCriteriaType.ACCEPTABLE.getReference());
            JSONObject content       = (JSONObject) jsonObject.get(StopCriteriaType.CONTENT.getReference());
            JSONObject structure     = (JSONObject) jsonObject.get(StopCriteriaType.STRUCTURE.getReference());

            boolean    isTime        = (boolean)    time.get(ACTIVE_KEYWORD);
            boolean    isGenerations = (boolean)    generations.get(ACTIVE_KEYWORD);
            boolean    isAcceptable  = (boolean)    acceptable.get(ACTIVE_KEYWORD);
            boolean    isContent     = (boolean)    content.get(ACTIVE_KEYWORD);
            boolean    isStructure   = (boolean)    structure.get(ACTIVE_KEYWORD);

            if(ConfigParserUtils.howManyTrue(isTime, isGenerations, isAcceptable, isContent, isStructure) != 1){
                return null;
            }

            if(isTime){
                long milliseconds    = ((Long) time.get("how-much-secs"))*1000;
                return new TimeStopCriteria(milliseconds);
            }

            if(isGenerations){
                int maxGenerations = ((Long) generations.get("how-many")).intValue();
                return new GenerationsStopCriteria(maxGenerations);
            }

            if(isAcceptable){
                double minFitness  = (double) acceptable.get("min-fitness");
                return new AcceptableStopCriteria(minFitness);
            }

            if(isContent){
                int stableGenerations = ((Long) content.get("stable-gens")).intValue();
                return new ContentStopCriteria(stableGenerations);
            }

            if(isStructure){
                JSONObject importantGenes = (JSONObject) structure.get("important-genes");
                int individuals           = ((Long)      structure.get("how-many-individuals")).intValue();
                int deltaGenerations      = ((Long)      structure.get("how-many-generations")).intValue();
                boolean height            = (boolean)    importantGenes.get("height");
                boolean weapons           = (boolean)    importantGenes.get("weapons");
                boolean boots             = (boolean)    importantGenes.get("boots");
                boolean gauntlets         = (boolean)    importantGenes.get("gauntlets");
                boolean chestPieces       = (boolean)    importantGenes.get("chestpieces");
                Set<GeneType> geneTypes   = new HashSet<>(5);
                if(height)
                    geneTypes.add(GeneType.HEIGHT);
                if(weapons)
                    geneTypes.add(GeneType.WEAPONS);
                if(boots)
                    geneTypes.add(GeneType.BOOTS);
                if(gauntlets)
                    geneTypes.add(GeneType.GAUNTLETS);
                if(chestPieces)
                    geneTypes.add(GeneType.CHESTPIECES);
                return new StructureStopCriteria(geneTypes, individuals, deltaGenerations);
            }
            return null;
        } catch (Exception e){
            return null;
        }
    }
}
