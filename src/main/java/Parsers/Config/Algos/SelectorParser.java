package Parsers.Config.Algos;

import ConcreteFitnessCalculators.ConcreteInverseFitnessFunction;
import ConcreteSelectors.*;
import Parsers.Config.ConfigParserUtils;
import Parsers.Parser;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class SelectorParser implements Parser<Collection<SelectionOperatorWrapper>> {

    final private static String   ELITE_KEYWORD                 = "elite";
    final private static String   ROULETTE_KEYWORD              = "roulette";
    final private static String   UNIVERSAL_KEYWORD             = "universal";
    final private static String   RANKING_KEYWORD               = "ranking";
    final private static String   BOLTZMANN_KEYWORD             = "boltzmann";
    final private static String   DETERMINISTIC_TOURNEY_KEYWORD = "deterministic-tourney";
    final private static String   PROBABILISTIC_TOURNEY_KEYWORD = "probabilistic-tourney";
    final private static String   ACTIVE_KEYWORD                = "active";
    final private static String   TEMPERATURE_KEYWORD           = "temperature";
    final private static String   THRESHOLD_KEYWORD             = "threshold";
    final private static String   PARTICIPANTS_KEYWORD          = "participants";

    final private static String   PONDERATION_KEYWORD           = "ponderation";
    final private static String   METHOD_KEYWORD                = "method";
    final private static String   VALUE_KEYWORD                 = "value";

    private static SelectorParser selectorParser = null;
    private JSONObject            jsonObject     = null;

    private SelectorParser(JSONObject jsonObject){
        this.jsonObject = jsonObject;
    }

    public static SelectorParser getInstance(JSONObject jsonObject){
        if(selectorParser == null)
            selectorParser = new SelectorParser(jsonObject);
        if(!selectorParser.jsonObject.equals(jsonObject))
            selectorParser.jsonObject = jsonObject;
        return selectorParser;
    }

    @Override
    public Collection<SelectionOperatorWrapper> parse() {
        try {
            JSONObject ponderation            = (JSONObject) jsonObject.get(PONDERATION_KEYWORD);
            String     method                 = (String)     ponderation.get(METHOD_KEYWORD);
            double     value                  = (double)     ponderation.get(VALUE_KEYWORD);
            boolean    elite                  = (boolean)    jsonObject.get(ELITE_KEYWORD);
            boolean    roulette               = (boolean)    jsonObject.get(ROULETTE_KEYWORD);
            boolean    universal              = (boolean)    jsonObject.get(UNIVERSAL_KEYWORD);
            boolean    ranking                = (boolean)    jsonObject.get(RANKING_KEYWORD);
            JSONObject boltzmann              = (JSONObject) jsonObject.get(BOLTZMANN_KEYWORD);
            JSONObject deterministicTourney   = (JSONObject) jsonObject.get(DETERMINISTIC_TOURNEY_KEYWORD);
            JSONObject probabilisticTourney   = (JSONObject) jsonObject.get(PROBABILISTIC_TOURNEY_KEYWORD);
            boolean    isBoltzmann            = (boolean)    boltzmann.get(ACTIVE_KEYWORD);
            boolean    isDeterministicTourney = (boolean)    deterministicTourney.get(ACTIVE_KEYWORD);
            boolean    isProbabilisticTourney = (boolean)    probabilisticTourney.get(ACTIVE_KEYWORD);
            switch (method){
                case ELITE_KEYWORD:
                    if(!elite)
                        return null;
                    break;
                case ROULETTE_KEYWORD:
                    if(!roulette)
                        return null;
                    break;
                case UNIVERSAL_KEYWORD:
                    if(!universal)
                        return null;
                    break;
                case RANKING_KEYWORD:
                    if(!ranking)
                        return null;
                    break;
                case BOLTZMANN_KEYWORD:
                    if (!isBoltzmann)
                        return null;
                    break;
                case DETERMINISTIC_TOURNEY_KEYWORD:
                    if(!isDeterministicTourney)
                        return null;
                    break;
                case PROBABILISTIC_TOURNEY_KEYWORD:
                    if(!isProbabilisticTourney)
                        return null;
                    break;
                default:
                    return null;
            }
            int amountInTrue = ConfigParserUtils.howManyTrue(elite, roulette, universal, ranking, isBoltzmann, isDeterministicTourney, isProbabilisticTourney);
            if (!(amountInTrue > 0 && amountInTrue < 3))
                return null;
            List<SelectionOperatorWrapper> selectionOperators = new ArrayList<>(2);
            if(elite) {
                double thisPonderation;
                if(method.equals(ELITE_KEYWORD))
                    thisPonderation = value;
                else
                    thisPonderation = 1 - value;
                selectionOperators.add(new SelectionOperatorWrapper(new EliteSelector(), thisPonderation));
            }
            if(roulette) {
                double thisPonderation;
                if(method.equals(ROULETTE_KEYWORD))
                    thisPonderation = value;
                else
                    thisPonderation = 1 - value;
                selectionOperators.add(new SelectionOperatorWrapper(new RuletteSelector(), thisPonderation));
            }
            if(universal) {
                double thisPonderation;
                if(method.equals(UNIVERSAL_KEYWORD))
                    thisPonderation = value;
                else
                    thisPonderation = 1 - value;
                selectionOperators.add(new SelectionOperatorWrapper(new UniversalSelector(), thisPonderation));
            }
            if(ranking) {
                double thisPonderation;
                if(method.equals(RANKING_KEYWORD))
                    thisPonderation = value;
                else
                    thisPonderation = 1 - value;
                selectionOperators.add(new SelectionOperatorWrapper(new RankingSelector(new ConcreteInverseFitnessFunction()), thisPonderation));
            }
            if(isBoltzmann){
                double thisPonderation;
                if(method.equals(BOLTZMANN_KEYWORD))
                    thisPonderation = value;
                else
                    thisPonderation = 1 - value;
                double temperature  = (double) boltzmann.get(TEMPERATURE_KEYWORD);
                selectionOperators.add(new SelectionOperatorWrapper(new BoltzmannSelector(temperature), thisPonderation));
            }
            if(isDeterministicTourney){
                double thisPonderation;
                if(method.equals(DETERMINISTIC_TOURNEY_KEYWORD))
                    thisPonderation = value;
                else
                    thisPonderation = 1 - value;
                int    participants = ((Long) deterministicTourney.get(PARTICIPANTS_KEYWORD)).intValue();
                selectionOperators.add(new SelectionOperatorWrapper(new DeterministicTournamentSelector(participants), thisPonderation));
            }
            if(isProbabilisticTourney){
                double thisPonderation;
                if(method.equals(PROBABILISTIC_TOURNEY_KEYWORD))
                    thisPonderation = value;
                else
                    thisPonderation = 1 - value;
                double threshold    = (double) probabilisticTourney.get(THRESHOLD_KEYWORD);
                selectionOperators.add(new SelectionOperatorWrapper(new ProbabilisticTournamentSelector(threshold), thisPonderation));
            }
            return selectionOperators;

        } catch (Exception e){
            return null;
        }
    }
}
