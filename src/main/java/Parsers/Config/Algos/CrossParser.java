package Parsers.Config.Algos;

import ConcreteCrossOperators.DoublePointCrossOperator;
import ConcreteCrossOperators.RingCrossOperator;
import ConcreteCrossOperators.SinglePointCrossOperator;
import ConcreteCrossOperators.UniformCrossOperator;
import Operators.CrossOperator;
import Parsers.Config.ConfigParserUtils;
import Parsers.Parser;
import org.json.simple.JSONObject;

public class CrossParser implements Parser<CrossOperator> {

    final private static String CROSS_ONCE_KEYWORD        = "once";
    final private static String CROSS_TWICE_KEYWORD       = "twice";
    final private static String CROSS_ANNULAR_KEYWORD     = "annular";
    final private static String CROSS_UNIFORM_KEYWORD     = "uniform";
    final private static String ACTIVE_OPERATOR_KEYWORD   = "active";
    final private static String ROTATION_CHAIN_KEYWORD    = "rot-chain-len";
    final private static String CROSS_PROBABILITY_KEYWORD = "cross-probability";

    private static CrossParser crossParser = null;
    private JSONObject         jsonObject  = null;

    private CrossParser(JSONObject jsonObject){
        this.jsonObject = jsonObject;
    }

    public static CrossParser getInstance(JSONObject jsonObject){
        if(crossParser == null)
            crossParser = new CrossParser(jsonObject);
        if(!crossParser.jsonObject.equals(jsonObject))
            crossParser.jsonObject = jsonObject;
        return crossParser;
    }

    @Override
    public CrossOperator parse() {
        try {
            JSONObject crossOnce       = (JSONObject) jsonObject.get(CROSS_ONCE_KEYWORD);
            JSONObject crossTwice      = (JSONObject) jsonObject.get(CROSS_TWICE_KEYWORD);
            JSONObject crossAnnular    = (JSONObject) jsonObject.get(CROSS_ANNULAR_KEYWORD);
            JSONObject crossUniform    = (JSONObject) jsonObject.get(CROSS_UNIFORM_KEYWORD);
            boolean    isCrossOnce     = (boolean) crossOnce.get(ACTIVE_OPERATOR_KEYWORD);
            boolean    isCrossTwice    = (boolean) crossTwice.get(ACTIVE_OPERATOR_KEYWORD);
            boolean    isCrossAnnular  = (boolean) crossAnnular.get(ACTIVE_OPERATOR_KEYWORD);
            boolean    isCrossUniform  = (boolean) crossUniform.get(ACTIVE_OPERATOR_KEYWORD);
            if(ConfigParserUtils.howManyTrue(isCrossOnce, isCrossTwice, isCrossAnnular, isCrossUniform) != 1)
                return null;
            if(isCrossOnce)
                return new SinglePointCrossOperator();
            if(isCrossTwice)
                return new DoublePointCrossOperator();
            if(isCrossAnnular){
                int    rotationChainLength = ((Long) crossAnnular.get(ROTATION_CHAIN_KEYWORD)).intValue();
                return new RingCrossOperator(rotationChainLength);
            }
            if(isCrossUniform){
                double crossProbability    = (double) crossUniform.get(CROSS_PROBABILITY_KEYWORD);
                return new UniformCrossOperator(crossProbability);
            }
            return null;
        }catch (Exception e){
            return null;
        }
    }
}
