package Parsers.Config;

import Parsers.Config.Equipment.EquipmentParser;
import Parsers.Config.Equipment.EquipmentPiece;
import Parsers.Config.Equipment.Inventory;
import Parsers.Parser;
import org.json.simple.JSONObject;

public class EquipmentFileParser implements Parser<Inventory> {

    private static final String        FILES_KEYWORD         = "files";
    private static final String        WEAPONS_KEYWORD       = "weapons";
    private static final String        BOOTS_KEYWORD         = "boots";
    private static final String        HELMETS_KEYWORD       = "helmets";
    private static final String        GAUNTLETS_KEYWORD     = "gauntlets";
    private static final String        CHESTPIECES_KEYWORD   = "chestpieces";

    private static EquipmentFileParser equipmentFileParser = null;
    private JSONObject                 config              = null;

    private EquipmentFileParser(JSONObject config){ this.config = config; }

    public static EquipmentFileParser getInstance(JSONObject config){
        if(equipmentFileParser == null)
            equipmentFileParser = new EquipmentFileParser(config);
        if(!equipmentFileParser.config.equals(config))
            equipmentFileParser.config = config;
        return equipmentFileParser;
    }

    private Inventory parseFiles(String weaponsFile, String bootsFile
            , String helmetsFile, String gauntletsFile, String chestpiecesFile){
        EquipmentPiece[] weapons, boots, helmets, gauntlets, chestpieces;
        EquipmentParser equipmentParser;
        equipmentParser             = EquipmentParser.getInstance(weaponsFile);
        weapons                     = equipmentParser.parse();
        equipmentParser             = EquipmentParser.getInstance(bootsFile);
        boots                       = equipmentParser.parse();
        equipmentParser             = EquipmentParser.getInstance(helmetsFile);
        helmets                     = equipmentParser.parse();
        equipmentParser             = EquipmentParser.getInstance(gauntletsFile);
        gauntlets                   = equipmentParser.parse();
        equipmentParser             = EquipmentParser.getInstance(chestpiecesFile);
        chestpieces                 = equipmentParser.parse();
        return new Inventory(weapons, boots, helmets, gauntlets, chestpieces);
    }

    @Override
    public Inventory parse() {
        try {
            JSONObject  files           = (JSONObject) config.get(FILES_KEYWORD);
            String      weaponsFile     = (String)     files.get(WEAPONS_KEYWORD);
            String      bootsFile       = (String)     files.get(BOOTS_KEYWORD);
            String      helmetsFile     = (String)     files.get(HELMETS_KEYWORD);
            String      gauntletsFile   = (String)     files.get(GAUNTLETS_KEYWORD);
            String      chestpiecesFile = (String)     files.get(CHESTPIECES_KEYWORD);

            return parseFiles(weaponsFile, bootsFile, helmetsFile, gauntletsFile, chestpiecesFile);
        }catch (Exception e){
            return null;
        }
    }
}
