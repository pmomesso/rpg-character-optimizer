package Parsers.Config;

import Individuals.Individual;
import Parsers.Config.Equipment.Inventory;
import Parsers.Parser;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileReader;
import java.util.List;

public class ConfigFileParser implements Parser<Input> {

    private static ConfigFileParser configFileParser = null;
    private String filepath;

    private ConfigFileParser(String filepath){
        this.filepath = filepath;
    }

    public static ConfigFileParser getInstance(String filepath){
        if(configFileParser == null)
            configFileParser = new ConfigFileParser(filepath);
        if(!configFileParser.filepath.equals(filepath))
            configFileParser.filepath = filepath;
        return configFileParser;
    }

    @Override
    public Input parse(){
        JSONParser jsonParser = new JSONParser();

        try {
            JSONObject jsonObject = (JSONObject) jsonParser.parse(new FileReader(this.filepath));

            EquipmentFileParser     equipmentFileParser     = EquipmentFileParser.getInstance(jsonObject);
            Inventory               inventory               = equipmentFileParser.parse();
            if (inventory == null)
                return null;

            AlgorithmsParser        algorithmsParser        = AlgorithmsParser.getInstance(jsonObject);
            GenerationOperators     generationOperators     = algorithmsParser.parse();
            if(generationOperators == null)
                return null;

            InitialPopulationParser initialPopulationParser = InitialPopulationParser.getInstance(jsonObject, inventory, generationOperators.getMutationOperator(), generationOperators.getCrossOperator());
            InitialPopulationParserResult initialPopulationParserResult = initialPopulationParser.parse();
            List<Individual> individuals                    = initialPopulationParserResult.getInitialPopulation();
            int initSize = initialPopulationParserResult.getSize();
            return new Input(inventory, generationOperators, individuals, initSize, initialPopulationParserResult.getFc());
        } catch (Exception e){
            return null;
        }
    }
}