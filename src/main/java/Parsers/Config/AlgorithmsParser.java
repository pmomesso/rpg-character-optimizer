package Parsers.Config;

import Operators.CrossOperator;
import Operators.MutationOperator;
import Operators.SelectionOperator;
import Parsers.Config.Algos.*;
import Parsers.Parser;
import StopCriteria.StopCriteria;
import org.json.simple.JSONObject;

import java.util.Collection;

public class AlgorithmsParser implements Parser<GenerationOperators> {

    final static private String                  ALGORITHMS_KEYWORD        = "algorithms";
    final static private String                  GEN_OP_KEYWORD            = "gen-op";
    final static private String                  CROSS_OP_KEYWORD          = "cross";
    final static private String                  MUTATION_OP_KEYWORD       = "mutation";
    final static private String                  PARENT_SELECTION_KEYWORD  = "parent-selection";
    final static private String                  NEW_GEN_SELECTION_KEYWORD = "new-gen-selection";
    final static private String                  IMPLEMENTATION_KEYWORD    = "implementation";
    final static private String                  STOP_CRITERIA_KEYWORD     = "stop";

    final static private String                  K_KEYWORD                 = "K";

    private static AlgorithmsParser algorithmsParser   = null;
    private JSONObject              jsonObject         = null;

    private AlgorithmsParser(JSONObject jsonObject){
        this.jsonObject = jsonObject;
    }

    public static AlgorithmsParser getInstance(JSONObject jsonObject){
        if(algorithmsParser == null)
            algorithmsParser = new AlgorithmsParser(jsonObject);
        if(!algorithmsParser.jsonObject.equals(jsonObject))
            algorithmsParser.jsonObject = jsonObject;
        return algorithmsParser;
    }

    @Override
    public GenerationOperators parse() {

        try {
            JSONObject    algorithms         = (JSONObject) jsonObject.get(ALGORITHMS_KEYWORD);
            int           K                  = ((Long)      algorithms.get(K_KEYWORD)).intValue();

            JSONObject    geneOperations     = (JSONObject) algorithms.get(GEN_OP_KEYWORD);

            JSONObject    crossOperator      = (JSONObject) geneOperations.get(CROSS_OP_KEYWORD);
            CrossParser   crossParser        = CrossParser.getInstance(crossOperator);
            CrossOperator finalCrossOperator = crossParser.parse();
            if(finalCrossOperator == null){
                return null;
            }

            JSONObject       mutationOperator      = (JSONObject) geneOperations.get(MUTATION_OP_KEYWORD);
            MutationParser   mutationParser        = MutationParser.getInstance(mutationOperator);
            MutationOperator finalMutationOperator = mutationParser.parse();
            if(finalMutationOperator == null){
                return null;
            }

            JSONObject                    parentSelector   = (JSONObject) algorithms.get(PARENT_SELECTION_KEYWORD);
            SelectorParser                selectorParser   = SelectorParser.getInstance(parentSelector);
            Collection<SelectionOperatorWrapper> parentSelections = selectorParser.parse();
            if(parentSelections == null) {
                return null;
            }

            JSONObject     newGenSelector                  = (JSONObject) algorithms.get(NEW_GEN_SELECTION_KEYWORD);
            selectorParser                                 = SelectorParser.getInstance(newGenSelector);
            Collection<SelectionOperatorWrapper> newGenSelections = selectorParser.parse();

            if(newGenSelections == null) {
                return null;
            }

            JSONObject    implementation              = (JSONObject) algorithms.get(IMPLEMENTATION_KEYWORD);
            ImplementationParser implementationParser = ImplementationParser.getInstance(implementation);
            EngineType engineType                     = implementationParser.parse();
            if(engineType == null) {
                return null;
            }

            JSONObject    stopCriteria            = (JSONObject) algorithms.get(STOP_CRITERIA_KEYWORD);
            StopCriteriaParser stopCriteriaParser = StopCriteriaParser.getInstance(stopCriteria);
            StopCriteria finalStopCriteria        = stopCriteriaParser.parse();
            if(finalStopCriteria == null)
                return null;

            return new GenerationOperators(engineType, finalCrossOperator, finalMutationOperator, parentSelections, newGenSelections, finalStopCriteria, K);
        } catch (Exception e){
            return null;
        }
    }
}
