package Parsers.Config;

import Individuals.Individual;
import Operators.FitnessCalculator;
import Parsers.Config.Equipment.Inventory;

import java.util.List;

public class Input {
    final private Inventory           inventory;
    final private GenerationOperators generationOperators;
    final private List<Individual>    initialPopulation;
    final private FitnessCalculator fc;
    final private int size;

    public Input(Inventory inventory, GenerationOperators generationOperators, List<Individual> initialPopulation, int size, FitnessCalculator fc){
        this.inventory           = inventory;
        this.generationOperators = generationOperators;
        this.initialPopulation   = initialPopulation;
        this.size = size;
        this.fc = fc;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public GenerationOperators getGenerationOperators() {
        return generationOperators;
    }

    public List<Individual> getInitialPopulation() {
        return initialPopulation;
    }

    public FitnessCalculator getFc() {
        return fc;
    }

    public int getSize() {
        return size;
    }
}
