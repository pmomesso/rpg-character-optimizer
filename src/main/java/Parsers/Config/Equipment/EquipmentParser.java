package Parsers.Config.Equipment;


import Parsers.Parser;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class EquipmentParser implements Parser<EquipmentPiece[]> {

    private static EquipmentParser equipmentParser = null;
    private String                 filepath;

    private EquipmentParser(String filepath){ this.filepath = filepath; }

    public static EquipmentParser getInstance(String filepath){
        if(equipmentParser == null)
            equipmentParser = new EquipmentParser(filepath);
        if(!equipmentParser.filepath.equals(filepath))
            equipmentParser.filepath = filepath;
        return equipmentParser;
    }

    @Override
    public EquipmentPiece[] parse() {
        File file = new File(filepath);
        try {
            Scanner scanner = new Scanner(file);
            List<EquipmentPiece> equipmentPieceList = new LinkedList<>();
            scanner.nextLine();
            while (scanner.hasNextBigInteger()) {
                BigInteger id   = scanner.nextBigInteger();
                BigDecimal str  = scanner.nextBigDecimal();
                BigDecimal agi  = scanner.nextBigDecimal();
                BigDecimal dex  = scanner.nextBigDecimal();
                BigDecimal def  = scanner.nextBigDecimal();
                BigDecimal vit  = scanner.nextBigDecimal();
                EquipmentPiece equipmentPiece = new EquipmentPiece(id, str, agi, dex, vit, def);
                equipmentPieceList.add(equipmentPiece);
            }
            return equipmentPieceList.toArray(new EquipmentPiece[0]);
        } catch (FileNotFoundException e){
            return null;
        }
    }
}
