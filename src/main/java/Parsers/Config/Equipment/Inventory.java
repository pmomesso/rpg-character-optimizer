package Parsers.Config.Equipment;

import ConcreteGenes.EquipmentGene;

public class Inventory {
    EquipmentGene[] weapons;
    EquipmentGene[] boots;
    EquipmentGene[] helmets;
    EquipmentGene[] gauntlets;
    EquipmentGene[] chestpieces;

    public Inventory(EquipmentPiece[] weapons, EquipmentPiece[] boots
            , EquipmentPiece[] helmets, EquipmentPiece[] gauntlets
            , EquipmentPiece[] chestpieces){
        EquipmentMapper equipmentMapper = EquipmentMapper.getInstance();
        this.weapons     = equipmentMapper.map(weapons);
        this.boots       = equipmentMapper.map(boots);
        this.helmets     = equipmentMapper.map(helmets);
        this.gauntlets   = equipmentMapper.map(gauntlets);
        this.chestpieces = equipmentMapper.map(chestpieces);
    }

    public EquipmentGene[] getWeapons() {
        return weapons;
    }

    public EquipmentGene[] getBoots() {
        return boots;
    }

    public EquipmentGene[] getHelmets() {
        return helmets;
    }

    public EquipmentGene[] getGauntlets() {
        return gauntlets;
    }

    public EquipmentGene[] getChestpieces() {
        return chestpieces;
    }
}
