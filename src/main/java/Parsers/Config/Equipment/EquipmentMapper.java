package Parsers.Config.Equipment;

import ConcreteGenes.EquipmentGene;

public class EquipmentMapper {

    private static EquipmentMapper equipmentMapper = null;

    private EquipmentMapper(){}

    public static EquipmentMapper getInstance(){
        if(equipmentMapper == null)
            equipmentMapper = new EquipmentMapper();
        return equipmentMapper;
    }

    public EquipmentGene[] map(EquipmentPiece[] equipmentPieces){
        EquipmentGene[] equipmentGenes = new EquipmentGene[equipmentPieces.length];
        for (int i = 0; i < equipmentPieces.length; i++){
            equipmentGenes[i] = new EquipmentGene(equipmentPieces[i].getId()
                    , equipmentPieces[i].getStr()
                    , equipmentPieces[i].getAgi()
                    , equipmentPieces[i].getDex()
                    , equipmentPieces[i].getDef()
                    , equipmentPieces[i].getVit());
        }
        for (int i = 0; i < equipmentPieces.length; i++){
            equipmentGenes[i].setPossibleValues(equipmentGenes);
        }
        return equipmentGenes;
    }
}
