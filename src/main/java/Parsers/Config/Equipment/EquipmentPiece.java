package Parsers.Config.Equipment;

import java.math.BigDecimal;
import java.math.BigInteger;

public class EquipmentPiece {
    final static private char SEPARATOR = ' ';
    final private BigInteger id;
    final private BigDecimal str;
    final private BigDecimal agi;
    final private BigDecimal dex;
    final private BigDecimal vit;
    final private BigDecimal def;

    public EquipmentPiece(BigInteger id, BigDecimal str, BigDecimal agi, BigDecimal dex, BigDecimal vit, BigDecimal def){
        this.id     = id;
        this.str    = str;
        this.agi    = agi;
        this.dex    = dex;
        this.vit    = vit;
        this.def    = def;
    }

    public int getId() {
        return id.intValue();
    }

    public double getStr() {
        return str.doubleValue();
    }

    public double getAgi() {
        return agi.doubleValue();
    }

    public double getDex() {
        return dex.doubleValue();
    }

    public double getVit() {
        return vit.doubleValue();
    }

    public double getDef() {
        return def.doubleValue();
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(id);
        stringBuilder.append(SEPARATOR);
        stringBuilder.append(str);
        stringBuilder.append(SEPARATOR);
        stringBuilder.append(agi);
        stringBuilder.append(SEPARATOR);
        stringBuilder.append(dex);
        stringBuilder.append(SEPARATOR);
        stringBuilder.append(vit);
        stringBuilder.append(SEPARATOR);
        stringBuilder.append(def);
        return stringBuilder.toString();
    }
}
